# stage 1: build
FROM node:13-alpine AS builder
# set working directory
WORKDIR /app
# install dependencies first so that this layer is cached by Docker
# then, subsequent builds where only source files are edited and no new dependencies are installed will be faster
COPY package.json yarn.lock ./
RUN yarn
# copy remaining source code to the container and build
COPY . .
RUN yarn build

# stage 2: serve
FROM node:13-alpine
# install serve
RUN yarn global add serve
# set working directory
WORKDIR /app
# copy build files over
COPY --from=builder /app/build .
# command to be run in the container
CMD ["serve", "-p", "80", "-s", "."]