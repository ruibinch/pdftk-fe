// page URLs
export const URL_HOME = '/home';
export const URL_TD = '/tenderiser';
export const URL_FS = '/factsheet';
export const URL_CHANGELOG = '/changelog';
export const URL_FEEDBACK = '/feedback';
export const URL_PDF2TXT = '/pdf2txt';
export const URL_DOC_TKFE = '/docs/tkfe';
export const URL_DOC_TKBE = '/docs/tkbe';
export const URL_DOC_TKDEPLOY = '/docs/tkdeploy';
// Module names
export const MODULE_HOME = 'Home';
export const MODULE_TD = 'Tenderiser';
export const MODULE_FS = 'Factsheet Searcher';
export const MODULE_CHANGELOG = 'Changelog';
export const MODULE_FEEDBACK = 'Feedback';
export const MODULE_PDF2TXT = 'PDF-to-text';
export const MODULE_DOC_TKFE = 'Documentation - PDF Toolkit Frontend';
export const MODULE_DOC_TKBE = 'Documentation - PDF Toolkit Backend';
export const MODULE_DOC_TKDEPLOY = 'Documentation - PDF Toolkit Deployment';

// General
export const STATUS_SUCCESS = 'success';
export const STATUS_FAIL = 'failed';
export const XHR_MSG_ERROR = 'Error connecting to server';
export const XHR_MSG_TIMEOUT = 'Connection to server timed out';
// Messages
export const MSG_UPLOAD_SUCCESS = 'Upload successful';
export const MSG_UPLOAD_ERROR = 'Error in file upload';
export const MSG_SEARCH_SUCCESS = 'Search successful';
export const MSG_SEARCH_ERROR = 'Error processing search';
export const MSG_ERROR_SERVER_CONN = 'Error connecting to server';
export const MSG_ERROR_CHANGELOG = 'Error obtaining changelog list';
export const MSG_ERROR_FEEDBACK = 'Error obtaining feedback list';
export const MSG_FEEDBACK_UPDATE_ERROR = 'Error updating feedback list';
export const MSG_FEEDBACK_SUBMIT_ERROR = 'Error submitting feedback entry';
export const MSG_FEEDBACK_SUBMIT_SUCCESS = 'Feedback entry submitted';
export const MSG_NO_ENTRIES = 'No entries found.';
export const MSG_PROCESSING = 'Processing...';
export const MSG_FIELD_REQ = 'This field is required.';
export const MSG_FIELDS_REQ = 'Please fill in the required fields';

// Strings
export const STR_ADD = 'add';
export const STR_ALL = 'All';
export const STR_ALL_OCCURRENCES = 'allOccurrences';
export const STR_BUG_REPORT = 'Bug Report';
export const STR_DATE = 'Date';
export const STR_DISLIKE = 'dislike';
export const STR_DOWNVOTE = 'Downvote';
export const STR_INDIVWORDS = 'indivWords';
export const STR_LIKE = 'like';
export const STR_NEXT = 'Next';
export const STR_PHRASE = 'phrase';
export const STR_POPULARITY = 'Popularity';
export const STR_PREFIX = 'prefix';
export const STR_PRESALES = 'Presales';
export const STR_PREVIOUS = 'Previous';
export const STR_REMOVE = 'remove';
export const STR_RESULTS = 'Results';
export const STR_SALES = 'Sales';
export const STR_SEARCH = 'Search';
export const STR_SUGGESTION = 'Suggestion';
export const STR_UPLOAD = 'Upload';
export const STR_UPVOTE = 'Upvote';