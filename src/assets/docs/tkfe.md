## Overview

The frontend component is developed using **ReactJS**. It is bootstrapped using [**Create React App**](https://create-react-app.dev/) and styled using [**Ant Design**](https://ant.design/docs/react/introduce).

## Structure

The `/src` folder contains the following 4 sub-folders:

1. `assets`
    - Documentation, images, overall configuration
1. `components`
    - React components; forms bulk of the codebase
1. `constants`
    - Common strings and keywords used throughout the project
1. `utils`
    - Utility functions used throughout the project

## Components

The `components` folder can be divided into 4 categories:

1. Layout
    - Components that depict the overall layout of the app

    | Folder | Purpose |
    | --- | --- |
    | `app` | Highest-level component; contains all routing to individual pages |
    | `header` | Header and help drawer contents |
    | `home` | Homepage of app |
    | `sidebar` | Sidebar menu contents |
    | `notfound` | Error 404 page when the desired page does not exist |

1. Tools/Pages
    - Each tool folder contains a hierarchy of sub-folders that map to the workflow of the tool

    | Folder | Purpose |
    | --- | --- |
    | `changelog` | Changelog page |
    | `factsheet` | **Factsheet Searcher** tool |
    | `feedback` | Feedback page |
    | `pdf2txt` | **PDF-to-text** tool |
    | `tenderiser` | **Tenderiser** tool |

1. Utilities
    - Common utility components that are used in multiple places across the app

    | Folder | Purpose |
    | --- | --- |
    | `dropzone` | Dropzone for file upload |
    | `steps` | Steps for representation of a tool's workflow |

1. Documentation
    - Components for rendering of documentation pages

    | Folder | Purpose |
    | --- | --- |
    | `docs` | Documentation components |

### Tools folder structure

The name of a tool folder should be the same as the URL endpoint used to access the tool page. For example, the component for the PDF-to-text tool is stored under the `/src/components/pdf2txt` folder and accessible via the `/pdf2txt` endpoint.

Component names of a tool should be prefixed with a short form of the tool name. In this app, the short-form mappings are:
- `TD` == `Tenderiser`
- `FS` == `Factsheet Searcher`
- `P2T` == `PDF-to-text`

Each tool has a workflow of steps, e.g. *Upload* step -> *Results* step. Each step here will be stored in its own sub-folder in the tool folder. The component name will follow the format `{COMPONENT_SHORT_FORM_NAME}{STEP_NAME}.js`, e.g. `P2TUpload.js`.

All tools will store a `{COMPONENT_SHORT_FORM_NAME}Main.js` file in the top-level of the tool folder hierarchy.

Thus, a sample folder structure for a tool goes as follows:

```
    components
    |--- aaabbbccc
    |    |--- upload
    |    |    |--- ABCUpload.js
    |    |    |--- ABCUpload.test.js
    |    |--- results
    |    |    |--- ABCResults.js
    |    |    |--- ABCResults.test.js
    |    |--- ABCMain.js
    |    |--- ABCMain.test.js
```

## Testing Framework

Jest is used as the testing framework. It collects all files ending in `.test.js` as the test cases to run.

To run the test suite, run the following command.

```
$ yarn test
```

To run the test suite and print a coverage report, run the following command.

```
$ yarn test --coverage --watchAll=false
```

Test cases should be segregated by component, where the name of the test case will follow the format **`{COMPONENT_NAME}.test.js`**. The test file will be placed in the same folder as the component.

Test cases for each component are divided into the following 4 categories and ordered similarly:

1. Rendering
    - Test that display elements have been rendered
1. Interactions
    - Test potential user actions that would modify the state and/or make a function call
    - Primarily `onClick` events
1. Lifecycle method invocations
    - Test execution of functions to be run on component mount/update
1. Functions
    - Test business logic of individual functions

## Misc Information

### Manual CSS definitions

Avoid manual CSS definitions as much as possible; utilise the Ant Design components instead.

In the event that manual definitions are really necessary, store all of them in the `/src/components/app/App.css` file. A common CSS file makes for easier monitoring of CSS modifications and prevents overlapping CSS.

### Documentation pages

Documentation pages are written in Markdown syntax and stored in the `/src/assets/docs` folder.

The `react-markdown` library is used to render the Markdown pages into React components and the `github-markdown-css` library is used to style it similarly to the GitHub Markdown style.