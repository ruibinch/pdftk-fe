let TKBE_IP = '34.87.187.144';
if (process.env.REACT_APP_ENV !== undefined) {
  if (process.env.REACT_APP_ENV === 'dev') {
    TKBE_IP = 'localhost:5005';
  }
}

export const SERVER_SETTINGS = {
  'ip': TKBE_IP,
};

export const SETTINGS_GUTTER = { xs: 8, sm: 16, md: 24, lg: 32 };