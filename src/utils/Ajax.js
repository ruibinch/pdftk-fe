import {
  STR_PHRASE,
  XHR_MSG_ERROR,
  XHR_MSG_TIMEOUT
} from './../constants/Constants';
import { SERVER_SETTINGS } from './../assets/Config';

/***********************************************************************************************/
/*                                        HELPER FUNCTIONS                                     */
/***********************************************************************************************/

function setXhrHandlers(xhr, resolve, reject) {
  xhr.onload = () => {
    const response = JSON.parse(xhr.response);
    // validation checks on response
    if (xhr.status === 200) {
      resolve(response['results']);
    } else {
      reject(response['error']);
    }
  }

  xhr.onerror = () => { reject(XHR_MSG_ERROR); }

  xhr.ontimeout = () => { reject(XHR_MSG_TIMEOUT); }
}

/***********************************************************************************************/
/*                                            API CALLS                                        */
/***********************************************************************************************/

export function getChangelog() {
  /**
   * GET request to the /changelog endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);
    
    const url = `http://${SERVER_SETTINGS.ip}/api/changelog`;
    xhr.open('GET', url);
    xhr.send();
  });
}

export function getFeedback() {
  /**
   * GET request to the /feedback endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);
    
    const url = `http://${SERVER_SETTINGS.ip}/api/feedback`;
    xhr.open('GET', url);
    xhr.send();
  });
}

export function putFeedback(feedback) {
  /**
   * PUT request to the /feedback endpoint.
   */

   return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);
    
    const formData = new FormData();
    formData.set('feedback', JSON.stringify(feedback));
    
    const url = `http://${SERVER_SETTINGS.ip}/api/feedback`;
    xhr.open('PUT', url);
    xhr.send(formData);
   });
}

export function getIndices() {
  /**
   * GET request to the /indices endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);

    const url = `http://${SERVER_SETTINGS.ip}/api/indices`;
    xhr.open('GET', url);
    xhr.send();
  });
}

export function postPdf2Txt(files) {
  /**
   * POST request to the /pdf2txt endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);

    const formData = new FormData();
    // PDF files
    files.forEach((file, idx) => {
      formData.append('file' + idx, file);
    });

    const url = `http://${SERVER_SETTINGS.ip}/api/pdf2txt`;
    xhr.open('POST', url);
    xhr.send(formData);
  });
}

export function postSearch(collectionName, files, searchList, matchCriteria) {
  /**
   * POST request to the /searchtd endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);
    
    const formData = new FormData();
    // Search keywords
    formData.set('collectionName', collectionName);
    formData.set('searchList', searchList);
    formData.set('matchCriteria', matchCriteria === undefined ? STR_PHRASE : matchCriteria);
    // PDF files
    files.forEach((file, idx) => {
      formData.append('file' + idx, file);
    });

    const url = `http://${SERVER_SETTINGS.ip}/api/searchtd`;
    xhr.open('POST', url);
    xhr.send(formData);
  });
}

export function postSearchFeedback(files) {
  /**
   * POST request to the /searchfs endpoint.
   */

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    setXhrHandlers(xhr, resolve, reject);
    
    const formData = new FormData();
    // PDF files
    files.forEach((file, idx) => {
      formData.append('file' + idx, file);
    });

    const url = `http://${SERVER_SETTINGS.ip}/api/searchfs`;
    xhr.open('POST', url);
    xhr.send(formData);
  });
}