import { manageFiles } from './FileUpload';
import { STR_ADD, STR_REMOVE } from './../constants/Constants';

let files;

beforeEach(() => {
  files = [
    {
      uid: 1,
      file: 'test file',
      name: 'test.pdf',
      status: 'done',
    },
  ];
});

describe('manageFiles function', () => {

  test('action == STR_ADD', () => {
    const fileNew = { 'name': 'test2.pdf' };
    const filesUpdatedExpected = [
      ...files, 
      {
        uid: 2,
        file: fileNew,
        name: 'test2.pdf',
        status: 'done',
      },
    ];
    
    const filesUpdated = manageFiles(files, [fileNew], STR_ADD);
    expect(filesUpdated).toEqual(filesUpdatedExpected);
  });

  test('action == STR_REMOVE', () => {
    const fileToRemove = { uid: 1, name: 'test.pdf' };
    const filesUpdated = manageFiles(files, [fileToRemove], STR_REMOVE);
    expect(filesUpdated).toEqual([]);
  });
});