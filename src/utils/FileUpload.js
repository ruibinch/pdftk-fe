import { STR_ADD, STR_REMOVE } from './../constants/Constants';

/** 
 * Helper class to manage file additions/removals from a list.
 * 
 * For the following components:
 * - FSUpload
 * - TDUpload
 */

export function manageFiles(files, filesNew, action) {
  /**
   * Adds/removes the files in the `filesNew` array to/from the `files` array.
   */

  if (action === STR_ADD) {
    let uidNext = 0;

    // Get the current list of files
    let filenames = files.map(file => {
      if (file.uid >= uidNext) uidNext = file.uid + 1;
      return file.name;
    });

    // Check that the file name does not already exist before adding the file
    filesNew.forEach(fileNew => {
      if (!filenames.includes(fileNew.name)) {
        files.push({
          uid: uidNext,
          file: fileNew,
          name: fileNew.name,
          status: 'done',
        });
        // update `uidNext` for every new file added
        uidNext += 1;
      }
    });
  } else if (action === STR_REMOVE) {
    const [fileToRemove] = filesNew;
    files = files.filter(file => file.uid !== fileToRemove.uid);
  }

  return files;
}