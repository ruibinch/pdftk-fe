import React from 'react';
import { shallow } from 'enzyme';
import FSUpload from './FSUpload';

const mockOnChangeActiveStep = jest.fn();
const mockOnChangeInProgress = jest.fn();
const mockOnChangeResultsRaw = jest.fn();

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    hidden: false,
    inProgress: false,
    onChangeActiveStep: mockOnChangeActiveStep,
    onChangeInProgress: mockOnChangeInProgress,
    onChangeResultsRaw: mockOnChangeResultsRaw,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FSUpload {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeActiveStep.mockClear();
  mockOnChangeInProgress.mockClear();
  mockOnChangeResultsRaw.mockClear();
});

describe('<FSUpload /> rendering', () => {

  it('should render 1 <Dropzone>', () => {
    expect(wrapper.find('Dropzone')).toHaveLength(1);
  });
  
  it('should render 1 <Upload>', () => {
    expect(wrapper.find('Upload')).toHaveLength(1);
  });
  
  it('should render 2 <Button>', () => {
    expect(wrapper.find('Button')).toHaveLength(2);
  });
});

describe('<FSUpload /> interactions', () => {

  it('should call handleChangeFiles function when clicking the Clear button', () => {
    spy = jest.spyOn(instance, 'handleChangeFiles');
    instance.forceUpdate();

    wrapper.find('Button').last().props().onClick();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('should not call handleChangeFiles function when clicking the Clear button, if inProgress is true', () => {
    wrapper.setProps({ inProgress: true });
    spy = jest.spyOn(instance, 'handleChangeFiles');
    instance.forceUpdate();

    wrapper.find('Button').last().props().onClick();
    expect(spy).toHaveBeenCalledTimes(0);
  });
});

describe('<FSUpload /> functions', () => {

  it('should call handleChangeFiles function when handleFilesAdded function is called', () => {
    const filesNew = [
      { 'name': 'test1.pdf' },
      { 'name': 'test2.pdf' },
    ];
    spy = jest.spyOn(instance, 'handleChangeFiles');
    instance.forceUpdate();

    instance.handleFilesAdded(filesNew);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call handleChangeFiles function when handleRemoveFile function is called', () => {
    const file = 'test.pdf';
    spy = jest.spyOn(instance, 'handleChangeFiles');
    instance.forceUpdate();

    instance.handleRemoveFile(file);
    expect(spy).toHaveBeenCalledTimes(1);
  });
});