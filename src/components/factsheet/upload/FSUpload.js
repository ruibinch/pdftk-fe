import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Divider, Typography, Upload, Button, message } from 'antd';

import Dropzone from './../../dropzone/Dropzone';
import { manageFiles } from './../../../utils/FileUpload';
import { postSearchFeedback } from './../../../utils/Ajax';
import { MSG_UPLOAD_SUCCESS, MSG_UPLOAD_ERROR, STR_ADD, STR_REMOVE, MODULE_FS, MSG_PROCESSING } from './../../../constants/Constants';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  inProgress: PropTypes.bool.isRequired,
  onChangeActiveStep: PropTypes.func.isRequired,
  onChangeInProgress: PropTypes.func.isRequired,
  onChangeResultsRaw: PropTypes.func.isRequired,
};

class FSUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      files: [],
    };
    
    this.handleChangeFiles = this.handleChangeFiles.bind(this);
    this.handleFilesAdded = this.handleFilesAdded.bind(this);
    this.handleRemoveFile = this.handleRemoveFile.bind(this);
    this.handleClearFiles = this.handleClearFiles.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeFiles(files) {
    this.setState({ files: files });
  }

  /***********************************************************************************************/
  /*                                        HELPER FUNCTIONS                                     */
  /***********************************************************************************************/

  handleFilesAdded(filesNew) {
    if (this.props.inProgress) return;

    let files = manageFiles([...this.state.files], filesNew, STR_ADD);
    this.handleChangeFiles(files);
  }

  handleRemoveFile(file)  {
    if (this.props.inProgress) return;

    let files = manageFiles([...this.state.files], [file], STR_REMOVE);
    this.handleChangeFiles(files);
  }

  handleClearFiles() {
    if (this.props.inProgress) return;

    this.handleChangeFiles([]);
  }
  
  /***********************************************************************************************/
  /*                                       AJAX METHODS                                          */
  /***********************************************************************************************/

  async uploadFiles() {
    this.props.onChangeInProgress(true);

    let files = this.state.files.map(fileItem => fileItem.file);
    const promise = postSearchFeedback(files);
    message.loading({ content: MSG_PROCESSING, key: MODULE_FS, duration: 0 });

    try {
      const output = await promise;
      message.success({ content: MSG_UPLOAD_SUCCESS, key: MODULE_FS });
      this.props.onChangeResultsRaw(output);
      this.props.onChangeActiveStep(1);
    } catch (e) {
      message.error({ content: MSG_UPLOAD_ERROR, key: MODULE_FS });
    }
    this.props.onChangeInProgress(false);
  }

  render() {
    return (
      <div hidden={this.props.hidden}>
        <Row>
          <Col span={18} offset={3}>
            <Typography.Text strong>File Upload</Typography.Text>
            <div class="pad-b-small"></div>
            <Dropzone
              disabled={this.props.inProgress}
              onFilesAdded={this.handleFilesAdded}
            />
            <div class="pad-b-med"></div>
            <Upload
              disabled={this.props.inProgress}
              fileList={this.state.files}
              onRemove={this.handleRemoveFile}
            />
          </Col>
        </Row>

        <Divider />
        <Row type="flex" justify="end">
          <Col span={18} pull={3}>
            <Button
              type="primary"
              className="float-right"
              disabled={this.props.inProgress}
              onClick={this.uploadFiles}
            >
              Confirm
            </Button>
            <Button
              className="float-right"
              style={{ marginRight: 16 }}
              disabled={this.props.inProgress}
              onClick={this.handleClearFiles}
            >
              Clear
            </Button>
          </Col>
        </Row>
        <div class="mg-b-large"></div>
      </div>
    );
  }
}

FSUpload.propTypes = propTypes;

export default FSUpload;