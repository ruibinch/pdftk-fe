import React from 'react';
import PropTypes from 'prop-types';
import { Table, Button, Tooltip, Typography } from 'antd';

const propTypes = {
  results: PropTypes.array,
  onUpdateCopyValue: PropTypes.func.isRequired,
};

const defaultProps = {
  results: [],
};

const columns = [
  {
    title: 'Filename',
    dataIndex: 'filename',
    key: 'filename',
    fixed: 'left',
    width: 120,
    render: value => <Typography.Text strong>{value}</Typography.Text>
  },
  { title: 'Customer', dataIndex: 'customer', key: 'customer' },
  { title: 'Project Name', dataIndex: 'projectName', key: 'projectName' },
  { title: 'ITT Ref No', dataIndex: 'ITTRefNo', key: 'ITTRefNo' },
  { title: 'Date Published', dataIndex: 'datePublished', key: 'datePublished' },
  { title: 'Date Closed', dataIndex: 'dateClosed', key: 'dateClosed' },
  { title: 'Contact Person', dataIndex: 'contactPerson', key: 'contactPerson' },
  { title: 'Contact Details', dataIndex: 'contactDetails', key: 'contactDetails' },
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 80,
    render: value => {
      const customer = value['customer'];
      const projectName = value['projectName'];
      const ITTRefNo = value['ITTRefNo'];
      const datePublished = value['datePublished'];
      const dateClosed = value['dateClosed'];
      const contactPerson = value['contactPerson'];
      const contactDetails = value['contactDetails'].replace(' ', '');
  
      // tabs as placeholder to skip columns when pasting into Excel
      const copyValue = `${customer}\t${projectName}\t${ITTRefNo}\t\t\t\t\t${datePublished}\t${dateClosed}\t\t\t${contactPerson}\t${contactDetails}`;
      return (
        <Tooltip
          placement="top"
          title={'Copied to clipboard'}
          trigger="click"
        >
          <Button
            icon="copy"
            onClick={() => copyToClipboard(copyValue)}
          />
        </Tooltip>
      );
    },
  }
];

const copyToClipboard = copyValue => {
  /* Helper function to copy a value to the clipboard */

  const dummy = document.createElement('textarea');
  // dummy.style.display = 'none';
  document.body.appendChild(dummy);

  dummy.setAttribute('id', 'dummy_id');
  document.getElementById('dummy_id').value = copyValue;
  dummy.select();
  document.execCommand('copy');
  document.body.removeChild(dummy);
}

function FSResultsTable({ results }) {
  return (
    <>
      <Table
        dataSource={results}
        columns={columns}
        scroll={{ x: 1300 }}
      />
    </>
  );
}

FSResultsTable.propTypes = propTypes;
FSResultsTable.defaultProps = defaultProps;

export default FSResultsTable;