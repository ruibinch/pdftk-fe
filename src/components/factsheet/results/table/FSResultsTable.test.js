import React from 'react';
import { shallow } from 'enzyme';
import FSResultsTable from './FSResultsTable';

const mockOnUpdateCopyValue = jest.fn();
let props, wrapper, instance, spy;

function createTestProps() {
  return {
    results: [],
    onUpdateCopyValue: mockOnUpdateCopyValue,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FSResultsTable {...props} />);
  instance = wrapper.instance();
});


afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnUpdateCopyValue.mockClear();
});

describe('<FSResultsTable /> rendering', () => {

  // it('should render 1 <Table />', () => {
  //   expect(wrapper.find('Table')).toHaveLength(2);
  // });

  it('dummy test', () => {
    expect(1).toBe(1);
  });
});
