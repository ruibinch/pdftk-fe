import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Divider, Button } from 'antd';

import FSResultsTable from './table/FSResultsTable';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  resultsRaw: PropTypes.object,
  onChangeActiveStep: PropTypes.func.isRequired,
};

const defaultProps = {
  resultsRaw: {},
};

class FSResults extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
    };

    this.handleChangeResults = this.handleChangeResults.bind(this);
    this.handleNavBack = this.handleNavBack.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeResults(results) {
    this.setState({ results: results });
  }

  handleNavBack() {
    this.props.onChangeActiveStep(-1);
  }

  /***********************************************************************************************/
  /*                                        HELPER METHODS                                       */
  /***********************************************************************************************/

  addNextLineChar(text) {
    const indexCharBreak = 10;
    return text.slice(0, indexCharBreak) + '\n' + text.slice(indexCharBreak);
  }

  processResults() {
    const { resultsRaw } = this.props; 

    let results = Object.keys(resultsRaw).map((filename, idx) => {
      let customer = resultsRaw[filename]['Customer'];
      let projectName = resultsRaw[filename]['Project Name'];
      let ITTRefNo = resultsRaw[filename]['ITT Ref No'];
      let datePublished = resultsRaw[filename]['Date Published'];
      let dateClosed = resultsRaw[filename]['Date Closed'];
      let contactPerson = resultsRaw[filename]['Contact Person'];
      let contactDetails = resultsRaw[filename]['Contact Details'].replace(' ', '');
  
      return {
        key: idx,
        filename: this.addNextLineChar(filename),
        customer: customer,
        projectName: projectName,
        ITTRefNo: ITTRefNo,
        datePublished: datePublished,
        dateClosed: dateClosed,
        contactPerson: contactPerson,
        contactDetails: contactDetails,
        placeholder: '',
      };
    });
  
    this.handleChangeResults(results);
  };

  /***********************************************************************************************/
  /*                                 COMPONENT LIFECCLE METHODS                                  */
  /***********************************************************************************************/

  componentDidUpdate(prevProps) {
    if (prevProps.resultsRaw !== this.props.resultsRaw) {
      this.processResults();
    }
  };


  render() {
    return (
      <div hidden={this.props.hidden}>
        <Row>
          <Col span={22} offset={1}>
            <FSResultsTable
              results={this.state.results}
              onUpdateCopyValue={copyValue => this.handleChangeCopyValue(copyValue)}
            />
          </Col>
        </Row>
        
        <Divider />
        <Row>
          <Col span={3} push={3}>
            <Button onClick={this.handleNavBack}>Back</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

FSResults.propTypes = propTypes;
FSResults.defaultProps = defaultProps;

export default FSResults;