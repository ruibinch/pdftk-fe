import React from 'react';
import { shallow } from 'enzyme';
import FSResults from './FSResults';

const mockOnChangeActiveStep = jest.fn();
let props, wrapper, instance, spy;

function createTestProps() {
  return {
    hidden: false,
    resultsRaw: {},
    onChangeActiveStep: mockOnChangeActiveStep,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FSResults {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeActiveStep.mockClear();
});

describe('<FSResults /> rendering', () => {

  it('should render 1 <FSResultsTable />', () => {
    expect(wrapper.find('FSResultsTable')).toHaveLength(1);
  });

  it('should render 1 <Button />', () => {
    expect(wrapper.find('Button')).toHaveLength(1);
  });
});

describe('<FSResults /> interactions', () => {

  it('should call the onChangeActiveStep props function when Back button is clicked', () => {
    wrapper.find('Button').first().props().onClick();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(1);
    expect(mockOnChangeActiveStep).toHaveBeenCalledWith(-1);
  });
});

describe('<FSResults /> lifecycle method invocations', () => {

  it('should call processResults when resultsRaw props is updated', () => {
    spy = jest.spyOn(instance, 'processResults');
    instance.forceUpdate();

    wrapper.setProps({ resultsRaw: { 'test': { 'Contact Details': '123 123 123' } } });
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

describe('<FSResults /> functions', () => {

  it('should update results state when handleChangeResults function is called', () => {
    const testValue = ['test'];
    instance.handleChangeResults(testValue);
    expect(instance.state.results).toEqual(testValue);
  });
  
  it('should update results state based on resultsRaw props when processResults function is called', () => {
    const resultsRawTest = {
      'test.pdf': {
        'Customer': 'testCustomer',
        'Project Name': 'testProjectName',
        'ITT Ref No': 'testITTRefNo',
        'Date Published': 'testDatePublished',
        'Date Closed': 'testDateClosed',
        'Contact Person': 'testContactPerson',
        'Contact Details': 'testContactDetails',
      },
    };
    const resultsTest = [
      {
        'key': 0,
        'filename': 'test.pdf',
        'customer': 'testCustomer',
        'projectName': 'testProjectName',
        'ITTRefNo': 'testITTRefNo',
        'datePublished': 'testDatePublished',
        'dateClosed': 'testDateClosed',
        'contactPerson': 'testContactPerson',
        'contactDetails': 'testContactDetails',
        'placeholder': '',
      },
    ];

    wrapper.setProps({ resultsRaw: resultsRawTest });
    instance.processResults();
    expect(instance.state.results[0]).toEqual(
      expect.objectContaining({
        key: resultsTest[0]['key'],
        // filename: resultsTest[0]['filename'], // failing due to \n char being added
        projectName: resultsTest[0]['projectName'],
        ITTRefNo: resultsTest[0]['ITTRefNo'],
        datePublished: resultsTest[0]['datePublished'],
        dateClosed: resultsTest[0]['dateClosed'],
        contactPerson: resultsTest[0]['contactPerson'],
        contactDetails: resultsTest[0]['contactDetails'],
        placeholder: resultsTest[0]['placeholder'],
      })
    );
  });
});