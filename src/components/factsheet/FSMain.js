import React from 'react';
import { Divider } from 'antd';

import { STR_UPLOAD, STR_RESULTS } from './../../constants/Constants';
import FSUpload from './upload/FSUpload';
import FSResults from './results/FSResults';
import StepsWrapper from './../steps/StepsWrapper';

class FSMain extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      stepActive: 0,
      steps: [
        { 'icon': 'upload', 'title': STR_UPLOAD },
        { 'icon': 'file-done', 'title': STR_RESULTS },
      ],
      inProgress: false,
      resultsRaw: {},
    };

    this.handleChangeActiveStep = this.handleChangeActiveStep.bind(this);
    this.handleChangeInProgress = this.handleChangeInProgress.bind(this);
    this.handleChangeResultsRaw = this.handleChangeResultsRaw.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeActiveStep(deltaStep) {
    let newStepActive = this.state.stepActive + deltaStep;
    this.setState({ stepActive: newStepActive });
  }

  handleChangeInProgress(inProgress) {
    this.setState({ inProgress: inProgress });
  }

  handleChangeResultsRaw(resultsRaw) {
    this.setState({ resultsRaw: resultsRaw });
  }

  render() {
    return (
      <>
        <StepsWrapper
          steps={this.state.steps}
          stepActive={this.state.stepActive}
          disabled={this.state.inProgress}
          onChangeStep={this.handleChangeActiveStep}
        />
        <Divider />
        <FSUpload
          hidden={this.state.stepActive !== 0}
          inProgress={this.state.inProgress}
          onChangeActiveStep={this.handleChangeActiveStep}
          onChangeInProgress={this.handleChangeInProgress}
          onChangeResultsRaw={this.handleChangeResultsRaw}
        />
        <FSResults
          hidden={this.state.stepActive !== 1}
          resultsRaw={this.state.resultsRaw}
          onChangeActiveStep={this.handleChangeActiveStep}
        />
      </>
    );
  }
}

export default FSMain;