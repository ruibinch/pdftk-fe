import React from 'react';
import PropTypes from 'prop-types';
import { Layout, PageHeader, Button, Tooltip, Drawer, Typography, Card, Icon,  } from 'antd';
import { MODULE_TD, MODULE_FS, MODULE_PDF2TXT } from './../../constants/Constants';

// drawer images
import fs_sample from './../../assets/img/fs_sample.png';
import fs_result from './../../assets/img/fs_result.png';
import td_sample from './../../assets/img/td_sample.png';
import td_search from './../../assets/img/td_search.png';

const propTypes = {
  page: PropTypes.string.isRequired,
};

const { Text } = Typography;

class Header extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      drawerVisible: false,
    };
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  showDrawer = () => this.setState({ drawerVisible: true });

  hideDrawer = () => this.setState({ drawerVisible: false });

  render() {
    const { page } = this.props;

    const subTitles = {
      [MODULE_TD]: 'Search through tender clauses effectively',
      [MODULE_FS]: 'Find the key information quickly',
      [MODULE_PDF2TXT]: 'Convert .pdf to .txt',
    };
    
    const styleHeader = {
      background: '#f4f4f4',
      borderBottom: '1px ridge rgba(212, 218, 223, 0.3)',
      boxShadow: '0 3px 8px 0 rgba(116, 129, 141, 0.1)',
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 16,
      paddingRight: 16,
    };

    const drawerContents = {
      [MODULE_TD]:
      <>
        <Text><strong>Step 1</strong>: Specify a unique collection name to utilise. A prompt will be displayed to indicate whether your input collection name is already existing in the Elasticsearch server.</Text><br/><br/>
        <Text>If you are uploading files to a new collection, ensure that your specified collection name is not already existing.</Text><br/><br/>
        <div class="mg-b-med"></div>
        <Text><strong>Step 2</strong>: Upload a tender document with a similar clause structure, e.g. <em>1.1 (clause entry)</em></Text>
        <div class="mg-b-small"></div>
        <Card cover={<img alt="td_sample" src={td_sample}/>}></Card>
        <div class="mg-b-small"></div>
        <Text>This step is not necessary if you are loading from an existing collection, unless you want to add new files to your pre-existing collection. </Text><br/><br/>
        <div class="mg-b-med"></div>
        <Text><strong>Step 3</strong>: Input your desired search keyword(s) and choose a search option.</Text><br/><br/>
        {/* <Text>Wordpacks contain frequently used keywords to serve as an easy way to add keywords.</Text> */}
        <Card cover={<img alt="td_search" src={td_search}/>}></Card>
        <div class="mg-b-med"></div>
        <Text><strong>Step 4</strong>: Clauses that contain your input search keyword(s) will be displayed in the results table.</Text><br/><br/>
        <Text>The individual clauses are extracted from the tender document on a best-effort basis.</Text>
      </>,
      [MODULE_FS]:
      <>
        <Text><strong>Step 1</strong>: Upload a PDF file of this structure</Text>
        <div class="mg-b-small"></div>
        <Card cover={<img alt="fs_sample" src={fs_sample}/>}></Card>
        <div class="mg-b-med"></div>
        <Text><strong>Step 2</strong>: View the extracted information</Text>
        <div class="mg-b-small"></div>
        <Card cover={<img alt="fs_result" src={fs_result}/>}></Card>
        <div class="mg-b-med"></div>
        <Text>Each row can be copied to the clipboard via the <Icon type="copy"/> button and subsequently pasted into the Excel template.</Text>
      </>,
      [MODULE_PDF2TXT]:
      <>
        <Text><strong>Step 1</strong>: Upload a PDF file(s). <br/>The maximum total file upload size is currently set at 50MB.</Text>
        <div class="mg-b-med"></div>
        <Text><strong>Step 2</strong>: View and/or download the extracted text of the files.</Text>
        <div class="mg-b-med"></div>
      </>,
    }
    
    return (
      <>
        <Drawer
          title="Help"
          placement="left"
          width={400}
          closable={true}
          visible={this.state.drawerVisible}
          onClose={this.hideDrawer}
        >
          {drawerContents[page]}
        </Drawer>
        <Layout.Header style={styleHeader}>
          <PageHeader
            style={{ paddingTop: 16, backgroundColor: 'transparent' }}
            title={page}
            subTitle={subTitles[page]}
            extra={
              <Tooltip placement="bottom" title={'Help'}>
                <Button
                  icon="question-circle"
                  style={{ color: '#222', borderColor: 'rgba(85, 85, 85, 0.8)' }}
                  hidden={!(page === MODULE_TD || page === MODULE_FS || page === MODULE_PDF2TXT)}
                  onClick={this.showDrawer}
                />
              </Tooltip>
            }
          />
        </Layout.Header>
      </>
    );
  }
}

Header.propTypes = propTypes;

export default Header;