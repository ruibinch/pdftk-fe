import React from 'react';
import { shallow } from 'enzyme';
import TDMain from './TDMain';

let wrapper, instance;

beforeEach(() => {
  wrapper = shallow(<TDMain />);
  instance = wrapper.instance();
});

describe('<TDMain /> rendering', () => {

  it('should render the following child components', () => {
    expect(wrapper.find('StepsWrapper')).toHaveLength(1);
    expect(wrapper.find('TDUpload')).toHaveLength(1);
    expect(wrapper.find('TDSearch')).toHaveLength(1);
    expect(wrapper.find('TDResults')).toHaveLength(1);
  });
});

describe('<TDMain /> functions', () => {

  it('should change stepActive state when handleChangeActiveStep is called', () => {
    wrapper.setState({ stepActive: 1 });
    instance.handleChangeActiveStep(1);
    expect(instance.state.stepActive).toEqual(2);
    instance.handleChangeActiveStep(-2);
    expect(instance.state.stepActive).toEqual(0);
  });

  it('should update inProgress state when handleChangeInProgress is called', () => {
    const testValue = true;
    instance.handleChangeInProgress(testValue);
    expect(instance.state.inProgress).toEqual(testValue);
  });

  it('should update connError state when handleChangeConnError is called', () => {
    const testValue = true;
    instance.handleChangeConnError(testValue);
    expect(instance.state.connError).toEqual(testValue);
  });

  it('should update collectionName state when handleChangeCollectionName is called', () => {
    const testValue = 'test';
    instance.handleChangeCollectionName(testValue);
    expect(instance.state.collectionName).toEqual(testValue);
  });

  it('should update files state when handleChangeFiles is called', () => {
    const testValue = ['test'];
    instance.handleChangeFiles(testValue);
    expect(instance.state.files).toEqual(testValue);
  });

  it('should update matchCriteria state when handleChangeMatchCriteria is called', () => {
    const testValue = 'test';
    instance.handleChangeMatchCriteria(testValue);
    expect(instance.state.matchCriteria).toEqual(testValue);
  });

  it('should update resultsRaw state when handleChangeResultsRaw is called', () => {
    const testValue = {'test': 'value'};
    instance.handleChangeResultsRaw(testValue);
    expect(instance.state.resultsRaw).toEqual(testValue);
  });
});