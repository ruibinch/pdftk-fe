import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Input, Icon } from 'antd';

const propTypes = {
  disabled: PropTypes.bool.isRequired,
  onAddToSearchList: PropTypes.func.isRequired,
  onDoSearch: PropTypes.func.isRequired,
};

class TDSearchInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
    };
    
    this.handleChangeSearchTerm = this.handleChangeSearchTerm.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
  }
  
  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/
    
  handleChangeSearchTerm(event) {
    this.setState({ searchTerm: event.target.value });
  }

  handleEnter() {
    let inputList = this.processSearchInput(this.state.searchTerm);
    this.props.onAddToSearchList(inputList);
    this.setState({ searchTerm: '' }); // reset input textbox
  }

  /***********************************************************************************************/
  /*                                    KEYWORD HANDLING METHODS                                 */
  /***********************************************************************************************/

  processSearchInput(input) {
    /**
     * Handles manual processing of search keywords.
     * E.g. stemming, extending it from the short form
     */

    let inputSplit = input.toLowerCase().split(',');

    // final list of parsed inputs
    let inputList = [];
    for (let word of inputSplit) {
      word = word.trim();

      switch (word) {
        case 'damage':
        case 'damages':
          inputList = [...inputList, 'damage', 'damages'];
          break;
        case 'ld':
        case 'liquidated damage':
        case 'liquidated damages':
          inputList = [...inputList, 'ld', 'liquidated damage', 'liquidated damages'];
          break;
        case 'pgp':
        case 'performance guarantee period':
          inputList = [...inputList, 'pgp', 'performance guarantee period'];
          break;
        default:
          inputList.push(word);
      }
    }

    return inputList;
  }

  render() {
    return (
      <>
        <Typography.Text>Enter search keyword(s), separated by commas:</Typography.Text>
        <div class="pad-b-small"></div>
        <Input
          disabled={this.props.disabled}
          prefix={<Icon type="search" style={{ color: 'rgba(0, 0, 0, .25)' }} />}
          value={this.state.searchTerm}
          onChange={this.handleChangeSearchTerm}
          onKeyPress={event => {
            if (!this.props.disabled) {
              if (event.ctrlKey) {
                this.props.onDoSearch();
              } else {
                if (event.key === 'Enter') this.handleEnter();
              }
            }
          }}
        />
      </>
    );
  };
}

TDSearchInput.propTypes = propTypes;

export default TDSearchInput;