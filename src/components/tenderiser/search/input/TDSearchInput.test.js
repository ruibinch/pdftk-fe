import React from 'react';
import { shallow } from 'enzyme';
import TDSearchInput from './TDSearchInput';

const mockOnAddToSearchList = jest.fn();
const mockOnDoSearch = jest.fn();

let props, wrapper, instance, spy;

function createTestProps(props) {
  return {
    disabled: false,
    onAddToSearchList: mockOnAddToSearchList,
    onDoSearch: mockOnDoSearch,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDSearchInput {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnAddToSearchList.mockClear();
  mockOnDoSearch.mockClear();
});

describe('<TDSearchInput /> rendering', () => {

  it('should render 1 <Input>', () => {
    expect(wrapper.find('Input')).toHaveLength(1);
  });
});

describe('<TDSearchInput /> interactions', () => {

  it('should update the searchTerm state value when there is a change in the search input', () => {
    spy = jest.spyOn(instance, 'handleChangeSearchTerm');
    instance.forceUpdate();
    const event = { 'target': { 'value': 'test' } };
    wrapper.find('Input').props().onChange(event);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(instance.state.searchTerm).toEqual('test');
  });

  it('when Enter is clicked, call onAddToSearchList props function and clear the existing input textbox', () => {
    wrapper.setState({ searchTerm: 'test' });
    const event = { 'key': 'Enter' };
    wrapper.find('Input').props().onKeyPress(event);

    expect(mockOnAddToSearchList).toHaveBeenCalledTimes(1);
    expect(mockOnAddToSearchList).toHaveBeenLastCalledWith(['test']);
    expect(instance.state.searchTerm).toEqual('');
  });

  // it('should call onTriggerSearch callback on a Ctrl+Enter key combination', () => {
  //     const event = {
  //         'ctrlKey': true,
  //         'key': 'Enter'
  //     };

  //     wrapper.find('input').props().onKeyPress(event);
  //     expect(mockTriggerSearch).toHaveBeenCalledTimes(1);
  //     expect(mockAddToSearchList).toHaveBeenCalledTimes(0);
  // });
});

describe('<TDSearchInput /> functions', () => {
  /**
   * Any of the individual input keywords in the `inputs` array 
   * should result in all permutations being added.
   */

  it('process input search keyword - damage', () => {
    let inputs = ['damage', 'damages'];

    inputs.forEach(input => {
      const output = instance.processSearchInput(input);
      expect(output).toEqual(inputs);
    });
  });
  
  it('process input search keyword - LD', () => {
    let inputs = ['ld', 'liquidated damage', 'liquidated damages'];

    inputs.forEach(input => {
      const output = instance.processSearchInput(input);
      expect(output).toEqual(inputs);
    });
  });
  
  it('process input search keyword - PGP', () => {
    let inputs = ['pgp', 'performance guarantee period'];

    inputs.forEach(input => {
      const output = instance.processSearchInput(input);
      expect(output).toEqual(inputs);
    });
  });
});