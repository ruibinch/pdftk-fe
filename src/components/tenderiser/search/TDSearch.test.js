import React from 'react';
import { shallow } from 'enzyme';
import TDSearch from './TDSearch';
import { STR_PHRASE } from './../../../constants/Constants';

const mockOnChangeInProgress = jest.fn();
const mockOnChangeMatchCriteria = jest.fn();
const mockOnChangeResultsRaw = jest.fn();
const mockOnChangeActiveStep = jest.fn();

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    hidden: false,
    disabled: false,
    collectionName: '',
    files: [],
    matchCriteria: STR_PHRASE,
    onChangeInProgress: mockOnChangeInProgress,
    onChangeMatchCriteria: mockOnChangeMatchCriteria,
    onChangeResultsRaw: mockOnChangeResultsRaw,
    onChangeActiveStep: mockOnChangeActiveStep,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDSearch {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  mockOnChangeInProgress.mockClear();
  mockOnChangeMatchCriteria.mockClear();
  mockOnChangeResultsRaw.mockClear();
  mockOnChangeActiveStep.mockClear();
  if (spy !== undefined) spy.mockClear();
});

describe('<TDSearch /> rendering', () => {

  it('should render the following child components', () => {
    expect(wrapper.find('TDSearchInput')).toHaveLength(1);
    expect(wrapper.find('TDSearchWordpacks')).toHaveLength(1);
    expect(wrapper.find('TDSearchOptions')).toHaveLength(1);
    expect(wrapper.find('TDSearchKeywords')).toHaveLength(1);
  });
});

describe('<TDSearch /> functions', () => {

  it('calling handleChangeSearchList should update the searchList state', () => {
    const searchList = ['test', 'search', 'list'];
    instance.handleChangeSearchList(searchList);
    expect(instance.state.searchList).toEqual(searchList);
  });

  it('calling handleNavBack should call onChangeActiveStep props function', () => {
    instance.handleNavBack();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(1);
    expect(mockOnChangeActiveStep).toHaveBeenCalledWith(-1);
  });

  it('calling handleNavBack should not call onChangeActiveStep props function, if disabled is true', () => {
    wrapper.setProps({ disabled: true });
    instance.handleNavBack();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(0);
  });

  it('calling handleAddToSearchList should update the searchList state', () => {
    const searchList = ['test', 'search', 'list'];
    instance.handleAddToSearchList(searchList);
    expect(instance.state.searchList).toEqual(searchList);
  });

  it('calling handleAddToSearchList should not update the searchList state, if disabled is true', () => {
    wrapper.setProps({ disabled: true });
    const searchList = ['test', 'search', 'list'];
    instance.handleAddToSearchList(searchList);
    expect(instance.state.searchList).toEqual([]);
  });

  it('calling handleRemoveFromSearchList should update the searchList state', () => {
    wrapper.setState({ searchList: ['test', 'search', 'list'] });
    instance.handleRemoveFromSearchList(['test', 'list']);
    expect(instance.state.searchList).toEqual(['search']);
  });

  it('calling handleRemoveFromSearchList should not update the searchList state, if disabled is true', () => {
    wrapper.setProps({ disabled: true });
    const searchList = ['test', 'search', 'list'];

    wrapper.setState({ searchList: searchList });
    instance.handleRemoveFromSearchList(['test', 'list']);
    expect(instance.state.searchList).toEqual(searchList);
  });

  it('validateInputParams should return false, when searchList is empty', () => {
    expect(instance.validateInputParams()).toEqual(false);
  });

  it('validateInputParams should return true, when searchList is not empty', () => {
    wrapper.setState({ searchList: ['sample'] });
    expect(instance.validateInputParams()).toEqual(true);
  });
});