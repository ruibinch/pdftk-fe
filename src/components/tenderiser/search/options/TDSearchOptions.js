import React from 'react';
import PropTypes from 'prop-types';
import { Radio, Typography, Icon, Popover } from 'antd';

import { STR_PHRASE, STR_INDIVWORDS, STR_PREFIX } from './../../../../constants/Constants';

const propTypes = {
  disabled: PropTypes.bool.isRequired,
  matchCriteria: PropTypes.string.isRequired,
  onChangeMatchCriteria: PropTypes.func.isRequired,
};

const radioStyle = {
  display: 'block',
  lineHeight: '1.5rem',
};

function TDSearchOptions({ disabled, matchCriteria, onChangeMatchCriteria }) {

  const contentPhrase = (
    <div>
      <span><strong>Search text:</strong></span><br/>
      <span>do the</span><br/>
      <br/>
      <span><strong>Text string:</strong></span><br/>
      <span>We will <mark>do the</mark> hokey-pokey.</span>
    </div>
  );
  
  const contentIndivWords = (
    <div>
      <span><strong>Search text:</strong></span><br/>
      <span>do the</span><br/>
      <br/>
      <span><strong>Text string:</strong></span><br/>
      <span>We will <mark>do</mark> all <mark>the</mark> hokey-pokey.</span>
    </div>
  );
  
  const contentPrefix = (
    <div>
      <span><strong>Search text:</strong></span><br/>
      <span>do the</span><br/>
      <br/>
      <span><strong>Text string:</strong></span><br/>
      <span>We will <mark>do the</mark>ir hokey-pokey.</span>
    </div>
  );

  const handleChangeOption = e => {
    if (!disabled) onChangeMatchCriteria(e.target.value);
  }

  return (
    <>
      <Typography.Text>Customise search option:&nbsp;&nbsp;</Typography.Text>
      <div class="pad-b-small"></div>
      <Radio.Group
        disabled={disabled}
        onChange={handleChangeOption}
        value={matchCriteria}
      >
        <Radio style={radioStyle} value={STR_PHRASE}>
          Match entire phrase &nbsp;
          <Popover content={contentPhrase}>
            <Icon type="eye" theme="filled" />
          </Popover>
        </Radio>
        <Radio style={radioStyle} value={STR_INDIVWORDS}>
          Match individual words &nbsp;
          <Popover content={contentIndivWords}>
            <Icon type="eye" theme="filled" />
          </Popover>
        </Radio>
        <Radio style={radioStyle} value={STR_PREFIX}>
          Match by prefix &nbsp;
          <Popover content={contentPrefix}>
            <Icon type="eye" theme="filled" />
          </Popover>
        </Radio>
      </Radio.Group>
    </>
  );
}

TDSearchOptions.propTypes = propTypes;

export default TDSearchOptions;