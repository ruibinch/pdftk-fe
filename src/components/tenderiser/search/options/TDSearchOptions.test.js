import React from 'react';
import { shallow } from 'enzyme';
import TDSearchOptions from './TDSearchOptions';
import { STR_PHRASE, STR_PREFIX } from './../../../../constants/Constants';

const mockOnChangeMatchCriteria = jest.fn();
let props, wrapper;

function createTestProps() {
  return {
    disabled: false,
    matchCriteria: STR_PHRASE,
    onChangeMatchCriteria: mockOnChangeMatchCriteria,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDSearchOptions {...props} />);
});

afterEach(() => {
  mockOnChangeMatchCriteria.mockClear();
});

describe('<TDSearchOptions /> rendering', () => {

  it('should render 1 <RadioGroup>', () => {
    expect(wrapper.find('RadioGroup')).toHaveLength(1);
  });

  it('should render 3 <Radio>', () => {
    expect(wrapper.find('Radio')).toHaveLength(3);
  });
});

describe('<TDSearchOptions /> interactions', () => {

  it('should call onChangeMatchCriteria props function when radio option is changed', () => {
    const event = { 'target': { 'value': STR_PREFIX }};
    wrapper.find('RadioGroup').props().onChange(event);

    expect(mockOnChangeMatchCriteria).toHaveBeenCalledTimes(1);
    // expect(wrapper.props.matchCriteria).toEqual(STR_PREFIX);
  });
  
  it('should not call onChangeMatchCriteria props function when radio option is changed, if disabled is true', () => {
    wrapper.setProps({ disabled: true });
    const event = { 'target': { 'value': STR_PREFIX }};
    wrapper.find('RadioGroup').props().onChange(event);

    expect(mockOnChangeMatchCriteria).toHaveBeenCalledTimes(0);
    // expect(wrapper.props.matchCriteria).toEqual(STR_PREFIX);
  });
});