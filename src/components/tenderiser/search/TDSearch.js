import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Divider, Button, message } from 'antd';

import { postSearch } from './../../../utils/Ajax';
import { MSG_SEARCH_SUCCESS, MSG_SEARCH_ERROR, MSG_PROCESSING, MODULE_TD } from './../../../constants/Constants';
import TDSearchKeywords from './keywords/TDSearchKeywords';
import TDSearchWordpacks from './wordpacks/TDSearchWordpacks';
import TDSearchOptions from './options/TDSearchOptions';
import TDSearchInput from './input/TDSearchInput';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  collectionName: PropTypes.string.isRequired,
  files: PropTypes.array,
  matchCriteria: PropTypes.string.isRequired,
  onChangeInProgress: PropTypes.func.isRequired,
  onChangeMatchCriteria: PropTypes.func.isRequired,
  onChangeResultsRaw: PropTypes.func.isRequired,
  onChangeActiveStep: PropTypes.func.isRequired,
};

const defaultProps = {
  files: [],
};

class TDSearch extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      searchList: [],
    };

    this.handleChangeSearchList = this.handleChangeSearchList.bind(this);
    this.handleNavBack = this.handleNavBack.bind(this);
    this.handleAddToSearchList = this.handleAddToSearchList.bind(this);
    this.handleRemoveFromSearchList = this.handleRemoveFromSearchList.bind(this);
    this.validateInputParams = this.validateInputParams.bind(this);
    this.doSearch = this.doSearch.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/
    
  handleChangeSearchList(searchList) {
    this.setState({ searchList: searchList });
  }

  handleNavBack() {
    if (this.props.disabled) return;
    this.props.onChangeActiveStep(-1);
  }

  /***********************************************************************************************/
  /*                                  SEARCH LIST MANAGEMENT METHODS                             */
  /***********************************************************************************************/

  handleAddToSearchList(wordList) {
    /* Adds the items in the `wordList` array to the search list. */
    if (this.props.disabled) return;

    let searchList = [...this.state.searchList, ...wordList] 
    searchList = [...new Set(searchList)]; // remove duplicates
    this.handleChangeSearchList(searchList);
  }

  handleRemoveFromSearchList(wordList) {
    /* Removes the items in the `wordList` array from the search list. */
    if (this.props.disabled) return;

    let searchList = [...this.state.searchList];
    searchList = searchList.filter(term => !wordList.includes(term));
    this.handleChangeSearchList(searchList);
  }

  /***********************************************************************************************/
  /*                                          HELPER METHODS                                     */
  /***********************************************************************************************/

  validateInputParams() {
    if (this.props.disabled) return false;
    
    if (!this.state.searchList.length) {
      message.error('Search list is empty');
      return false;
    }

    return true;
  }

  async doSearch() {
    if (!this.validateInputParams()) return;
    this.props.onChangeInProgress(true);

    let files = this.props.files.map(fileItem => fileItem.file);
    const promise = postSearch(
      this.props.collectionName,
      files,
      this.state.searchList,
      this.props.matchCriteria,
    );
    message.loading({ content: MSG_PROCESSING, key: MODULE_TD, duration: 0 });

    try {
      const output = await promise;
      message.success({ content: MSG_SEARCH_SUCCESS, key: MODULE_TD });
      this.props.onChangeResultsRaw(output);
      this.props.onChangeActiveStep(1);
    } catch (e) {
      message.error({ content: MSG_SEARCH_ERROR, key: MODULE_TD });
    }
    this.props.onChangeInProgress(false);
  }

  
  render() {
    return (
      <div hidden={this.props.hidden}>
        <Row>
          <Col span={10} offset={1}>
            <TDSearchInput
              disabled={this.props.disabled}
              onAddToSearchList={this.handleAddToSearchList}
              onDoSearch={this.doSearch}
            />
            <div class="pad-b-large"></div>
            <TDSearchWordpacks
              disabled={this.props.disabled}
              onAddToSearchList={this.handleAddToSearchList}
              onRemoveFromSearchList={this.handleRemoveFromSearchList}
            />
            <div class="pad-b-large"></div>
            <TDSearchOptions
              disabled={this.props.disabled}
              matchCriteria={this.props.matchCriteria}
              onChangeMatchCriteria={this.props.onChangeMatchCriteria}
            />
          </Col>
          <Col span={12} offset={1}>
            <TDSearchKeywords
              searchList={this.state.searchList}
              onRemoveFromSearchList={this.handleRemoveFromSearchList}
            />
          </Col>
        </Row>

        <Divider />
        <Row type="flex" justify="end">
          <Col span={16} pull={4}>
            <Button
              type="primary"
              className="float-right"
              disabled={this.props.disabled}
              onClick={this.doSearch}
            >
              Next
            </Button>
            <Button
              className="float-right"
              style={{ marginRight: 16 }}
              disabled={this.props.disabled}
              onClick={this.handleNavBack}
            >
              Back
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

TDSearch.propTypes = propTypes;
TDSearch.defaultProps = defaultProps;

export default TDSearch;