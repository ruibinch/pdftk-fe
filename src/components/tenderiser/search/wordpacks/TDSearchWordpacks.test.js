import React from 'react';
import { shallow } from 'enzyme';
import TDSearchWordpacks from './TDSearchWordpacks';
import { STR_PRESALES, STR_SALES } from './../../../../constants/Constants';
import { WORDPACKS } from './../../../../constants/Keywords';

const mockOnAddToSearchList = jest.fn();
const mockOnRemoveFromSearchList = jest.fn();
let props, wrapper;

function createTestProps() {
  return {
    disabled: false,
    onAddToSearchList: mockOnAddToSearchList,
    onRemoveFromSearchList: mockOnRemoveFromSearchList,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDSearchWordpacks {...props} />);
});

afterEach(() => {
  mockOnAddToSearchList.mockClear();
  mockOnRemoveFromSearchList.mockClear();
});

describe('<TDSearchWordpacks /> rendering', () => {

  it('should render 2 <CheckableTag>', () => {
    expect(wrapper.find('CheckableTag')).toHaveLength(2);
  });
});

describe('<TDSearchWordpacks /> interactions', () => {

  it('should add keyword to search list when a wordpack tag is selected', () => {
    wrapper.find('CheckableTag').first().props().onChange(true);
    expect(mockOnAddToSearchList).toHaveBeenCalledTimes(1);
    expect(mockOnAddToSearchList).toHaveBeenCalledWith(WORDPACKS[STR_PRESALES]);
  });

  it('should remove keyword from search list when a wordpack tag is de-selected', () => {
    wrapper.find('CheckableTag').last().props().onChange(false);
    expect(mockOnRemoveFromSearchList).toHaveBeenCalledTimes(1);
    expect(mockOnRemoveFromSearchList).toHaveBeenCalledWith(WORDPACKS[STR_SALES]);
  });
});