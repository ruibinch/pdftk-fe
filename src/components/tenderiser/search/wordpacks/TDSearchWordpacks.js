import React from 'react';
import PropTypes from 'prop-types';
import { Tag, Typography } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';

import { STR_PRESALES, STR_SALES } from './../../../../constants/Constants';
import { WORDPACKS } from './../../../../constants/Keywords';

const propTypes = {
  disabled: PropTypes.bool.isRequired,
  onAddToSearchList: PropTypes.func.isRequired,
  onRemoveFromSearchList: PropTypes.func.isRequired,
};

const tags = [STR_PRESALES, STR_SALES];

class TDSearchWordpacks extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTags: [],
    };

    this.handleToggleTag = this.handleToggleTag.bind(this);
  }

  handleToggleTag(tag, checked) {
    if (this.props.disabled) return;

    // update tag display
    const { selectedTags } = this.state;
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag);
    this.setState({ selectedTags: nextSelectedTags });

    // update search list
    if (checked) {
      this.props.onAddToSearchList(WORDPACKS[tag]);
    } else {
      this.props.onRemoveFromSearchList(WORDPACKS[tag]);
    }
  }

  render() {
    return (
      <>
        <Typography.Text>Load from wordpack:</Typography.Text>
        <div class="pad-b-small"></div>
        <TweenOneGroup
          enter={{
            scale: 0.8,
            opacity: 0,
            type: 'from',
            duration: 100,
            onComplete: e => e.target.style = ''
          }}
          leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
          appear={false}
        >
          {tags.map(tag => (
            <Tag.CheckableTag
              className={this.state.selectedTags.indexOf(tag) > -1 ? 'tag-wordpack-checked' : 'tag-wordpack'}
              key={tag}
              checked={this.state.selectedTags.indexOf(tag) > -1}
              onChange={checked => this.handleToggleTag(tag, checked)}
            >
              {tag}
            </Tag.CheckableTag>
          ))}
        </TweenOneGroup>
      </>
    );
  }
}

TDSearchWordpacks.propTypes = propTypes;

export default TDSearchWordpacks;