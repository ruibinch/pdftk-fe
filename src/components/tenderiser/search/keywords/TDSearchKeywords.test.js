import React from 'react';
import { shallow } from 'enzyme';
import TDSearchKeywords from './TDSearchKeywords';

const mockOnRemoveFromSearchList = jest.fn();
let props, wrapper;

function createTestProps() {
  return {
    searchList: ['some', 'sample', 'search', 'keywords'],
    onRemoveFromSearchList: mockOnRemoveFromSearchList,
  };
}

beforeAll(() => {
  props = createTestProps();
  wrapper = shallow(<TDSearchKeywords {...props} />);
});

afterEach(() => {
  mockOnRemoveFromSearchList.mockClear();
});

describe('<TDSearchKeywords /> rendering', () => {

  it('should render same number of <Tag>s as the length of searchList', () => {
      expect(wrapper.find('Tag')).toHaveLength(props.searchList.length);
  });
});

describe('<TDSearchKeywords /> interactions', () => {

  it('should call the onRemoveFromSearchList props function upon closing/removing an entry', () => {
      const mockedEvent = { preventDefault: () => { } }
      wrapper.find('Tag').first().props().onClose(mockedEvent);
      expect(mockOnRemoveFromSearchList).toHaveBeenCalledTimes(1);
  });
});