import React from 'react';
import PropTypes from 'prop-types';
import { Tag } from 'antd';

const propTypes = {
  searchList: PropTypes.array.isRequired,
  onRemoveFromSearchList: PropTypes.func.isRequired,
};

function TDSearchKeywords({ searchList, onRemoveFromSearchList }) {
  return (
    <>
      {searchList && searchList.map(keyword => (
        <Tag
          key={keyword}
          className="tag-keyword"
          color="volcano"
          closable
          onClose={e => {
            e.preventDefault();
            onRemoveFromSearchList([keyword]);
          }}
        >
          {keyword}
        </Tag>
      ))}
    </>
  );
}

TDSearchKeywords.propTypes = propTypes;

export default TDSearchKeywords;