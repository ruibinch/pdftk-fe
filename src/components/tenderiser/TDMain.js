import React from 'react';
import { Divider, message } from 'antd';

import StepsWrapper from './../steps/StepsWrapper';
import TDUpload from './upload/TDUpload';
import TDSearch from './search/TDSearch';
import TDResults from './results/TDResults';
import {
  MSG_ERROR_SERVER_CONN,
  STR_PHRASE,
  STR_RESULTS,
  STR_SEARCH,
  STR_UPLOAD
} from './../../constants/Constants';

class TDMain extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      stepActive: 0,
      steps: [
        { 'icon': 'upload', 'title': STR_UPLOAD },
        { 'icon': 'search', 'title': STR_SEARCH },
        { 'icon': 'file-done', 'title': STR_RESULTS },
      ],
      connError: null,
      inProgress: false,
      collectionName: '',
      searchList: [],
      files: [],
      matchCriteria: STR_PHRASE,
      resultsRaw: {},
    };

    this.handleChangeActiveStep = this.handleChangeActiveStep.bind(this);
    this.handleChangeInProgress = this.handleChangeInProgress.bind(this);
    this.handleChangeConnError = this.handleChangeConnError.bind(this);
    this.handleChangeCollectionName = this.handleChangeCollectionName.bind(this);
    this.handleChangeFiles = this.handleChangeFiles.bind(this);
    this.handleChangeMatchCriteria = this.handleChangeMatchCriteria.bind(this);
    this.handleChangeResultsRaw = this.handleChangeResultsRaw.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeActiveStep(deltaStep) {
    let newStepActive = this.state.stepActive + deltaStep;
    this.setState({ stepActive: newStepActive });
  }
  
  handleChangeInProgress(inProgress) {
    this.setState({ inProgress: inProgress });
  }
  
  handleChangeConnError(connError) {
    this.setState({ connError: connError });
  }

  handleChangeCollectionName(collectionName) {
    this.setState({ collectionName: collectionName });
  }

  handleChangeFiles(files) {
    this.setState({ files: files });
  }
  
  handleChangeMatchCriteria(matchCriteria) {
    this.setState({ matchCriteria: matchCriteria });
  }

  handleChangeResultsRaw(resultsRaw) {
    this.setState({ resultsRaw: resultsRaw });
  }

  /***********************************************************************************************/
  /*                              COMPONENT LIFECYCLE METHODS                                    */
  /***********************************************************************************************/

  componentDidUpdate(prevProps, prevState) {
    if (this.state.connError !== prevState.connError) {
      if (this.state.connError) {
        message.error(MSG_ERROR_SERVER_CONN);
      }
    }
  }

  render() {
    return (
      <>
        <StepsWrapper
          steps={this.state.steps}
          stepActive={this.state.stepActive}
          disabled={this.state.inProgress}
          onChangeStep={this.handleChangeActiveStep}
        />
        <Divider />
        <TDUpload
          hidden={this.state.stepActive !== 0}
          collectionName={this.state.collectionName}
          files={this.state.files}
          connError={this.state.connError}
          inProgress={this.state.inProgress}
          onChangeConnError={this.handleChangeConnError}
          onChangeCollectionName={this.handleChangeCollectionName}
          onChangeFiles={this.handleChangeFiles}
          onChangeActiveStep={this.handleChangeActiveStep}
        />
        <TDSearch
          hidden={this.state.stepActive !== 1}
          disabled={this.state.connError !== false || this.state.inProgress}
          collectionName={this.state.collectionName}
          files={this.state.files}
          matchCriteria={this.state.matchCriteria}
          onChangeInProgress={this.handleChangeInProgress}
          onChangeMatchCriteria={this.handleChangeMatchCriteria}
          onChangeResultsRaw={this.handleChangeResultsRaw}
          onChangeActiveStep={this.handleChangeActiveStep}
        />
        <TDResults
          hidden={this.state.stepActive !== 2}
          collectionName={this.state.collectionName}
          matchCriteria={this.state.matchCriteria}
          resultsRaw={this.state.resultsRaw}
          onChangeActiveStep={this.handleChangeActiveStep}
        />
      </>
    );
  }
}

export default TDMain;