import React from 'react';
import { shallow } from 'enzyme';
import TDUpload from './TDUpload';

let props, wrapper, instance, spy;

const mockOnChangeConnError = jest.fn();
const mockOnChangeCollectionName = jest.fn();
const mockOnChangeFiles = jest.fn();
const mockOnChangeActiveStep = jest.fn();

function createTestProps() {
  return {
    hidden: false,
    collectionName: '',
    files: [],
    connError: false,
    inProgress: false,
    onChangeConnError: mockOnChangeConnError,
    onChangeCollectionName: mockOnChangeCollectionName,
    onChangeFiles: mockOnChangeFiles,
    onChangeActiveStep: mockOnChangeActiveStep,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDUpload {...props}/>);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeConnError.mockClear();
  mockOnChangeCollectionName.mockClear();
  mockOnChangeFiles.mockClear();
  mockOnChangeActiveStep.mockClear();
});

describe('<TDUpload /> rendering', () => {

  it('should render 1 <Form>', () => {
    expect(wrapper.find('Form')).toHaveLength(1);
  });

  // it('should render 2 <Item>', () => {
  //   expect(wrapper.find('Item')).toHaveLength(2);
  // });

  it('should render 1 <Tooltip>', () => {
    expect(wrapper.find('Tooltip')).toHaveLength(1);
  });
  
  it('should render 1 <Input>', () => {
    expect(wrapper.find('Input')).toHaveLength(1);
  });

  it('should render 1 <Dropzone>', () => {
    expect(wrapper.find('Dropzone')).toHaveLength(1);
  });

  it('should render 1 <Upload>', () => {
    expect(wrapper.find('Upload')).toHaveLength(1);
  });

  it('should render 1 <Button>', () => {
    expect(wrapper.find('Button')).toHaveLength(1);
  });
});

describe('<TDUpload /> interactions', () => {

  it('should call the onChangeCollectionName props function when there is a change in the \'Collection Name\' text box', () => {
    const event = { 'target': { 'value': 'test' } }
    wrapper.find('Input').first().props().onChange(event);
    expect(mockOnChangeCollectionName).toHaveBeenCalledTimes(1);
  });

  it('should not call the onChangeCollectionName props function when there is a change in the \'Collection Name\' text box, but inProgress props is true', () => {
    wrapper.setProps({ inProgress: true });
    const event = { 'target': { 'value': 'test' } }
    wrapper.find('Input').first().props().onChange(event);
    expect(mockOnChangeCollectionName).toHaveBeenCalledTimes(0);
  });

  it('should call the \'onChangeFiles\' props function upon the addition of new files', () => {
    const filesNew = [{'name': 'sample1.pdf'}, {'name': 'sample2.pdf'}];

    instance.handleFilesAdded(filesNew);
    expect(mockOnChangeFiles).toHaveBeenCalledTimes(1);
  });

  it('if 3 files are added, of which 1 is already existing in the list, then there should ultimately be only 3 files', () => {
    const filesNew = [{'name': 'sample1.pdf'}, {'name': 'sample2.pdf'}, {'name': 'sample3.pdf'}];
    wrapper.setProps({ files: [{'name': 'sample1.pdf'}] });
    expect(instance.props.files).toHaveLength(1);

    instance.handleFilesAdded(filesNew);
    expect(mockOnChangeFiles).toHaveBeenCalledTimes(1);
  });

  it('should call the onChangeFile props function upon the removal of files', () => {
    const fileRemove = {'name': 'sample1.pdf'};
    wrapper.setProps({ files: [{'name': 'sample1.pdf'}] });
    expect(instance.props.files).toHaveLength(1);

    instance.handleRemoveFile(fileRemove);
    expect(mockOnChangeFiles).toHaveBeenCalledTimes(1);
    expect(mockOnChangeFiles).toHaveBeenLastCalledWith([]);
  });

  it('should call the onChangeActiveStep props function when the "Next" button is clicked', () => {
    wrapper.setState({ collections: 'test' });
    wrapper.setProps({ collectionName: 'test' });

    instance.handleClickNext();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(1);
    expect(mockOnChangeActiveStep).toHaveBeenLastCalledWith(1);
  });

  it('should not call the onChangeActiveStep props function when the "Next" button is clicked, if the collection name is invalid', () => {
    instance.handleClickNext();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(0);
  });
});

describe('<TDUpload /> lifecycle method invocations', () => {

  it('should call the populateCollectionsList function when the component is mounted', () => {
    spy = jest.spyOn(TDUpload.prototype, 'populateCollectionsList');
    wrapper = shallow(<TDUpload {...props} />);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call the populateCollectionsList function when there is a change in the inProgress props value', () => {
    wrapper.setProps({ inProgress: true });
    expect(instance.props.inProgress).toBe(true);
    wrapper.setProps({ inProgress: false });
    expect(instance.populateCollectionsList).toBeCalled();
  });
});
