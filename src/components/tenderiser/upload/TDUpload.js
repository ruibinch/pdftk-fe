import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Divider, Upload, Button, Form, Input, Tooltip, message } from 'antd';

import { getIndices } from './../../../utils/Ajax';
import Dropzone from './../../dropzone/Dropzone';
import { manageFiles } from './../../../utils/FileUpload';
import { STR_ADD, STR_REMOVE } from './../../../constants/Constants';

/**
 * TDUpload component contains 2 input fields:
 * 1. Collection Name
 * 2. File Upload
 * 
 * These are the possible permutations of source input:
 * 1. "Collection Name" only
 *    - Load from a pre-existing ES collection
 * 2. Both "Collection Name" and "File Upload"
 *    a. If collection name exists
 *      - Add the uploaded file(s) to the pre-existing collection
 *    b. If collection name does not exist
 *      - Upload the file(s) to a new collection with the specified name
 */

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  collectionName: PropTypes.string.isRequired,
  files: PropTypes.array.isRequired,
  connError: PropTypes.bool,
  inProgress: PropTypes.bool.isRequired,
  onChangeConnError: PropTypes.func.isRequired,
  onChangeCollectionName: PropTypes.func.isRequired,
  onChangeFiles: PropTypes.func.isRequired,
  onChangeActiveStep: PropTypes.func.isRequired,
};

const defaultProps = {
  connError: false,
};

class TDUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collections: [],
    };
    
    this.handleChangeCollectionName = this.handleChangeCollectionName.bind(this);
    this.handleChangeFiles = this.handleChangeFiles.bind(this);
    this.handleFilesAdded = this.handleFilesAdded.bind(this);
    this.handleRemoveFile = this.handleRemoveFile.bind(this);
    this.handleClickNext = this.handleClickNext.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeCollectionName(event) {
    if (this.props.inProgress) return;

    try {
      this.props.onChangeCollectionName(event.target.value);
    } catch (e) { }
  }

  handleChangeFiles(files) {
    this.props.onChangeFiles(files);
  }

  /***********************************************************************************************/
  /*                                        HELPER FUNCTIONS                                     */
  /***********************************************************************************************/

  handleFilesAdded(filesNew) {
    if (this.props.inProgress) return;

    let files = manageFiles([...this.props.files], filesNew, STR_ADD);
    this.handleChangeFiles(files);
  }

  handleRemoveFile(file)  {
    if (this.props.inProgress) return;

    let files = manageFiles([...this.props.files], [file], STR_REMOVE);
    this.handleChangeFiles(files);
  }

  handleClickNext() {
    /**
     * Event handler for clicking of "Next" button
     */

    // Check for the situation where "Collection Name" is not valid and there are no uploaded files
    if (this.props.collectionName === '') {
      // if "Collection Name" field is empty
      message.error('Please specify a collection name');
    } else {
      if (!this.state.collections.includes(this.props.collectionName)
          && this.props.files.length === 0
      ) {
        // if this is a new collection, with no uploaded files
        message.error('Please upload at least one file to a new collection');
      } else {
        this.props.onChangeActiveStep(1);
      }
    }
  }

  /***********************************************************************************************/
  /*                                          AJAX CALLS                                         */
  /***********************************************************************************************/
  
  async populateCollectionsList() {
    /* Pulls the list of existing indexes in the Elasticsearch server. */

    const promise = getIndices();

    try {
      const results = await promise;

      this.setState({ collections: results });
      this.props.onChangeConnError(false);
    } catch (e) {
      this.props.onChangeConnError(true);
    }
  }

  /***********************************************************************************************/
  /*                              COMPONENT LIFECYCLE METHODS                                    */
  /***********************************************************************************************/

  componentDidMount() {
    this.populateCollectionsList();
  }

  componentDidUpdate(prevProps) {
    // re-populate the indices list when this page is changed to active
    if (this.props.hidden !== prevProps.hidden) {
      if (this.props.hidden === false && prevProps.hidden === true) {
        this.populateCollectionsList();
      }
    }
  }


  render() {
    const formItemLayout = {
      labelCol: { span: 4, offset: 3 },
      wrapperCol: { span: 12 },
    };

    return (
      <div hidden={this.props.hidden}>
        <Form {...formItemLayout}>
          <Form.Item
            label="Collection Name"
            validateStatus={this.state.collections.includes(this.props.collectionName) ? 'success' : ''}
            help={
              this.props.collectionName === '' || this.props.connError !== false ?
              '' : 
              this.state.collections.includes(this.props.collectionName) ?
              'Existing collection found' : 'A new collection will be created with this name'
            }
            hasFeedback
            required
          >
            <Tooltip placement="topRight" title="Specify a collection name to load from or to save uploaded files to.">
              <Input
                id="collectionName"
                disabled={this.props.inProgress}
                value={this.props.collectionName}
                onChange={this.handleChangeCollectionName}
              />
            </Tooltip>
          </Form.Item>
          <div class="pad-b-small"></div>

          <Form.Item label="File Upload">
            <Dropzone
              disabled={this.props.inProgress}
              onFilesAdded={this.handleFilesAdded}
            />
            <div class="pad-b-med"></div>
            <Upload
              disabled={this.props.inProgress}
              fileList={this.props.files}
              onRemove={this.handleRemoveFile}
            />
          </Form.Item>
        </Form>

        <Divider />
        <Row type="flex" justify="end">
          <Col span={3} pull={5}>
            <Button
              type="primary"
              className="float-right"
              disabled={this.props.connError !== false || this.props.inProgress}
              onClick={this.handleClickNext}
            >
              Next
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

TDUpload.propTypes = propTypes;
TDUpload.defaultProps = defaultProps;

export default TDUpload;