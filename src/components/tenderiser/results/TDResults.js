import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Divider, Button } from 'antd';

import TDResultsTable from './table/TDResultsTable';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  collectionName: PropTypes.string.isRequired,
  matchCriteria: PropTypes.string.isRequired,
  resultsRaw: PropTypes.object.isRequired,
  onChangeActiveStep: PropTypes.func.isRequired,
};

class TDResults extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
    };

    this.handleNavBack = this.handleNavBack.bind(this);
    this.processRawResults = this.processRawResults.bind(this);
    this.sortResults = this.sortResults.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/
  
  handleNavBack() {
    this.props.onChangeActiveStep(-1);
  }

  /***********************************************************************************************/
  /*                                  RESULTS PROCESSING METHODS                                 */
  /***********************************************************************************************/

  processRawResults() {
    /**
     * Updates the `results` state variable with the latest results.
     * Processes the raw results into an array for display.
     */

    const { resultsRaw } = this.props;
    let results = [];
    
    for (let keyword in resultsRaw) {
      let keywordResults = resultsRaw[keyword];
      if (!Array.isArray(keywordResults) || !keywordResults.length) continue;

      const keywordResultsParsed = keywordResults.map(element => {
        return {
          'keyword': keyword,
          'filename': element['filename'],
          'clause': element['highlight'].replace(/’/g, '\''),
          // 'score': element['score'].toFixed(3),
          // 'highlight': element['highlight'],
        };
      });

      results.push(...keywordResultsParsed);
    };

    this.sortResults(results);
  }

  sortResults(results) {
    /* Sorts `results` by keyword, then filename, then clause number. */

    results.sort((a, b) => {
      // by keyword
      if (a.keyword !== b.keyword) {
        return a.keyword > b.keyword ? 1 : -1;
      } else {
        // by filename
        if (a.filename !== b.filename) {
          return a.filename > b.filename ? 1 : -1;
        } else {
          // by clause number
          const aSplit = a.clause.split('.');
          const bSplit = b.clause.split('.');
          
          // get first number
          let aa = parseInt(aSplit[0]);
          let bb = parseInt(bSplit[0]);
          let i = 1;
          while (aa === bb && (!isNaN(aa) && !isNaN(bb))) {
            // if both are still the same number, then go to the child number
            aa = parseInt(aSplit[i]);
            bb = parseInt(bSplit[i]);
            i++;
          }

          // aa is smaller
          if (isNaN(aa)) return -1;
          // bb is smaller
          if (isNaN(bb)) return 1;
          
          return aa > bb ? 1 : -1;
        }
      }
    });

    // Add an order number
    let resultsNumbered = results.map((e, idx) => {
      e['key'] = idx + 1;
      return e;
    });

    this.setState({ results: resultsNumbered });
  }

  /***********************************************************************************************/
  /*                                  COMPONENT LIFECYCLE METHODS                                */
  /***********************************************************************************************/

  componentDidUpdate(prevProps) {
    if (this.props.resultsRaw !== prevProps.resultsRaw) {
      this.processRawResults();
    }
  }

  render() {
    return (
      <div hidden={this.props.hidden}>
        <Row>
          <Col span={22} offset={1}>
            <TDResultsTable
              hidden={this.props.hidden}
              results={this.state.results}
              matchCriteria={this.props.matchCriteria}
            />
          </Col>
        </Row>

        <Divider />
        <Row>
          <Col span={3} push={4}>
            <Button onClick={this.handleNavBack}>
              Back
            </Button>
          </Col>
        </Row>
      </div>
    );
  };
}

TDResults.propTypes = propTypes;

export default TDResults;