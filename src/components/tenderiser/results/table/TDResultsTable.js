import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';
import { Table, Input, Icon, Modal } from 'antd';

import { STR_INDIVWORDS, STR_PREFIX } from './../../../../constants/Constants';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  results: PropTypes.array.isRequired,
  matchCriteria: PropTypes.string.isRequired,
};

class TDResultsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filterText: '',
      // modalVisible: false,
      // filenameSel: '',
      // clauseNumSel: '',
      // neighborClauseList: [],
    };
  };
  
  /***********************************************************************************************/
  /*                                        MODAL METHODS                                        */
  /***********************************************************************************************/

  showModal = () => this.setState({ modalVisible: true });

  hideModal = () => this.setState({ modalVisible: false });

  // Search on filter keyword
  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ filterText: selectedKeys[0] });
  };

  clearFilterText = () => {
    this.setState({ filterText: '' });
  }

  /***********************************************************************************************/
  /*                                  "KEYWORD" COLUMN METHODS                                   */
  /***********************************************************************************************/

  // Column props for dropdown filter in "Keyword" column
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Filter ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        // Sets the auto-focus on the input box
        setTimeout(() => this.searchInput.select());
      }
    },
  });

  /***********************************************************************************************/
  /*                                  "CLAUSE" COLUMN METHODS                                    */
  /***********************************************************************************************/

  // Column props for highlighting in "Clause" column 
  getColumnRenderHighlightProps = () => ({
    render: (value, record) => {
      // If match criteria is not "Individual Words", combine adjacent highlighted words
      if (this.props.matchCriteria !== STR_INDIVWORDS) {
        value = value.replace(/<\/mark> <mark>/g, ' ');
      }

      // To find words between <mark>...</mark> tags, then remove the tags
      const re = /(?<=<mark>)(.*?)(?=<\/mark>)/g;
      let matches = value.match(re);
      value = value.replace(/<mark>/g, '').replace(/<\/mark>/g, '');

      // If match criteria is "Prefix", truncate the regex match to only take the length of the search keyword
      // As the regex match encompasses the entire word(s) in the text instead of just the prefix match
      if (this.props.matchCriteria === STR_PREFIX) {
        matches = matches.map(e => e.substr(0, record['keyword'].length));
      }

      return (
        <Highlighter
          highlightStyle={{ backgroundColor: '#FFC40D', padding: 0 }}
          searchWords={matches || []}
          autoEscape
          findChunks={this.findChunksCustom}
          textToHighlight={value}
        />
      );
    }
  });
  
  findChunksCustom = ({ searchWords, textToHighlight }) => {
    let chunks = [];
    if (this.props.matchCriteria === STR_INDIVWORDS) {
      chunks = this.findChunksWholeWords(searchWords, textToHighlight);
    } else {
      chunks = this.findChunksAll(searchWords, textToHighlight);
    }

    return chunks;
  };
  
  // Finds chunks of entire words to match the search words
  findChunksWholeWords = (searchWords, textToHighlight) => {
    const chunks = [];
    const textWordsSplit = textToHighlight.split(' ');

    // It could be possible that there are multiple spaces between words
    // Hence we store the index (position) of each single word with textToHighlight
    let fromIndex = 0;
    const textWordsSplitWithPos = textWordsSplit.map(w => {
      const indexInWord = textToHighlight.indexOf(w, fromIndex);
      fromIndex = indexInWord;
      return {
        word: w,
        index: indexInWord,
      };
    });

    // Add chunks for each searchWord
    searchWords.forEach(sw => {
      textWordsSplitWithPos.forEach(s => {
        if (sw === s.word) {
          const start = s.index;
          const end = s.index + sw.length;
          chunks.push({ start, end });
        }
      });
    });

    return chunks;
  };

  // Finds all possible chunks to match the search words
  findChunksAll = (searchWords, textToHighlight) => {
    const chunks = [];
    let match;

    searchWords.forEach(sw => {
      let swEsc = this.escapeRegExp(sw);
      const re = new RegExp(swEsc, 'g');
      while ((match = re.exec(textToHighlight)) != null) {
        const start = match.index;
        const end = match.index + sw.length;
        chunks.push({ start, end });
        // next search will begin from the index of the end of this found string
        re.lastIndex = end; 
      }
    });

    return chunks;
  };

  // Helper function to add escape blackslashes before using the string in a RegExp object
  escapeRegExp = string => {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

  /***********************************************************************************************/
  /*                                  COMPONENT LIFECYCLE METHODS                                */
  /***********************************************************************************************/

  // FIXME: filterText state clearing but filter word in table is still present
  componentDidUpdate(prevProps) {
    // reset filter text when page is changed from active -> inactive
    if (this.props.hidden !== prevProps.hidden) {
      if (this.props.hidden === true && prevProps.hidden === false) {
        this.clearFilterText();
      }
    }
  }

  render() {
    const columns = [
      {
        title: 'No.',
        dataIndex: 'key',
        key: 'sn',
        width: '8%',
      },
      {
        title: 'Keyword',
        dataIndex: 'keyword',
        key: 'keyword',
        width: '15%',
        ...this.getColumnSearchProps('keyword'),
      },
      {
        title: 'Filename',
        dataIndex: 'filename',
        key: 'filename',
        width: '20%',
      },
      {
        title: 'Clause',
        dataIndex: 'clause',
        key: 'clause',
        ...this.getColumnRenderHighlightProps(),
      },
      // {
      //   title: 'View Neighbouring Clauses',
      //   dataIndex: '',
      //   key: 'view-neighbouring-clauses',
      //   width: '10%',
      //   render: () => <a onClick={this.showModal}>View</a>
      // },
    ];

    return (
      <>
        <Table
          dataSource={this.props.results}
          columns={columns}   
        />
        <Modal
          title="View Neighbouring Clauses"
          visible={this.state.modalVisible}
          onCancel={this.hideModal}
          footer={null}
        >
          <p>Content</p>  
        </Modal>
      </>
    );
  };
}

TDResultsTable.propTypes = propTypes;

export default TDResultsTable;

