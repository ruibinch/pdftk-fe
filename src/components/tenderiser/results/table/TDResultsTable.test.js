import React from 'react';
import { shallow } from 'enzyme';
import TDResultsTable from './TDResultsTable';
import { STR_PHRASE } from './../../../../constants/Constants';

let props, wrapper, instance;

function createTestProps() {
  return {
    hidden: false,
    results: [
      {
        'sn': '1',
        'keyword': 'here',
        'filename': 'sample.pdf',
        'clause': 'I am here',
      },
      {
        'sn': '2',
        'keyword': 'here',
        'filename': 'sample2.pdf',
        'clause': 'I am not here',
      },
    ],
    matchCriteria: STR_PHRASE,
  };
}

beforeAll(() => {
    props = createTestProps();
    wrapper = shallow(<TDResultsTable {...props} />);
    instance = wrapper.instance();
});

describe('<TDResultsTable /> rendering', () => {

// FIXME: table tests are failing recently for some reason
  // it('should render 1 <Table>', () => {
  //   expect(wrapper.find('Table')).toHaveLength(1);
  // });
  
  it('should render 1 <Modal>', () => {
    expect(wrapper.find('Modal')).toHaveLength(1);
  });
});

describe('<TDResultsTable /> lifecycle method invocations', () => {

  it('should reset the filterText state when there is a change in the hidden props value from true -> false', () => {
    wrapper.setState({ filterText: 'testFilterText' });

    wrapper.setProps({ hidden: true });
    expect(instance.props.hidden).toBe(true);
    wrapper.setProps({ hidden: false });
    expect(instance.state.filterText).toEqual('');
  });
});