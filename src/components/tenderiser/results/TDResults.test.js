import React from 'react';
import { shallow } from 'enzyme';
import TDResults from './TDResults';
import { STR_PHRASE } from './../../../constants/Constants';

const mockOnChangeActiveStep = jest.fn();
let props, wrapper, instance, spy;

function createTestProps() {
  return {
    hidden: false,
    collectionName: '',
    matchCriteria: STR_PHRASE,
    resultsRaw: {},
    onChangeActiveStep: mockOnChangeActiveStep,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<TDResults {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeActiveStep.mockClear();
});

describe('<TDResults /> rendering', () => {

  it('should render 1 <TDResultsTable />', () => {
    expect(wrapper.find('TDResultsTable')).toHaveLength(1);
  });

  it('should render 1 <Button />', () => {
    expect(wrapper.find('Button')).toHaveLength(1);
  });
});

describe('<TDResults /> interactions', () => {

  it('should call the onChangeActiveStep props function when Back button is clicked', () => {
    wrapper.find('Button').first().props().onClick();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(1);
    expect(mockOnChangeActiveStep).toHaveBeenCalledWith(-1);
  });
});

describe('<TDResults /> lifecycle method invocations', () => {

  it('should call processRawResults when resultsRaw props is updated', () => {
    spy = jest.spyOn(instance, 'processRawResults');
    instance.forceUpdate();

    wrapper.setProps({ resultsRaw: { 'test': 'sample' }});
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
