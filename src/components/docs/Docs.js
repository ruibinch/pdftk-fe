import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import 'github-markdown-css';
import { Row, Col } from 'antd';

import DocTkfe from './../../assets/docs/tkfe.md';
import DocTkbe from './../../assets/docs/tkbe.md';
import DocTkdeploy from './../../assets/docs/tkdeploy.md';
import { MODULE_DOC_TKFE, MODULE_DOC_TKBE, MODULE_DOC_TKDEPLOY } from '../../constants/Constants';

const propTypes = {
  page: PropTypes.oneOf([MODULE_DOC_TKFE, MODULE_DOC_TKBE, MODULE_DOC_TKDEPLOY]).isRequired,
};

class Docs extends React.Component {
  constructor(props) {
    super(props);

    this.state = { docs: null };
  }

  getDocPage() {
    const { page } = this.props;
    let docPage = null;

    switch (page) {
      case MODULE_DOC_TKFE:
        docPage = DocTkfe;
        break;
      case MODULE_DOC_TKBE:
        docPage = DocTkbe;
        break;
      case MODULE_DOC_TKDEPLOY:
        docPage = DocTkdeploy;
        break;
      default:
        break;
    }

    return docPage;
  }

  componentDidMount() {
    fetch(this.getDocPage())
      .then(res => res.text())
      .then(text => this.setState({ docs: text }));
  }

  render() {
    return (
      <Row>
        <Col span={18} offset={3}>
          <div class="markdown-body">
            <ReactMarkdown
              source={this.state.docs}
              transformImageUri={uri => 
                uri.startsWith('http') ? uri : `${process.env.PUBLIC_URL}../${uri}`
              }
            />
          </div>
        </Col>
      </Row>
    );
  }
}

Docs.propTypes = propTypes;

export default Docs;