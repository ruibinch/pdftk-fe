import React from 'react';
import { shallow } from 'enzyme';
import Docs from './Docs';
import { MODULE_DOC_TKFE, MODULE_DOC_TKBE, MODULE_DOC_TKDEPLOY } from './../../constants/Constants';

let wrapper;

function createTestProps() {
  return {
    page: MODULE_DOC_TKFE,
  };
}

beforeEach(() => {
  wrapper = shallow(<Docs {...createTestProps()} />);
});

describe('<Docs /> rendering', () => {
  it('should render 1 <ReactMarkdown />', () => {
    expect(wrapper.find('ReactMarkdown')).toHaveLength(1);
  });

  it('should render correctly when props is valid', () => {
    wrapper.setProps({ page: MODULE_DOC_TKBE });
    expect(wrapper.find('ReactMarkdown')).toHaveLength(1);

    wrapper.setProps({ page: MODULE_DOC_TKDEPLOY });
    expect(wrapper.find('ReactMarkdown')).toHaveLength(1);
  });

  it('should throw an error if props is invalid', () => {
    const t = () => {
      wrapper.setProps({ page: 'randomPageThatWillThrowAnError' });
    }

    expect(t).toThrow(Error);
  });
});