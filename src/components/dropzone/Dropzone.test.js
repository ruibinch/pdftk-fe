import React from 'react';
import { shallow } from 'enzyme';
import Dropzone from './Dropzone';

const mockOnFilesAdded = jest.fn(); 

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    disabled: false,
    onFilesAdded: mockOnFilesAdded,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<Dropzone {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
});

describe('<Dropzone /> rendering', () => {

  it('should render 2 <input>', () => {
    expect(wrapper.find('input')).toHaveLength(2);
  });
});

describe('<Dropzone /> interactions', () => {
 
  it('should change the \'highlight\' state when an item is being dragged over and then out of the dropzone', () => {
    wrapper.find('.dropzone').first().props().onDragOver();
    expect(wrapper.state('highlight')).toEqual(true);
    wrapper.find('.dropzone').first().props().onDragLeave();
    expect(wrapper.state('highlight')).toEqual(false);
  });

  it('should change the \'highlight\' state when an item is being dragged over and then dropped into the dropzone', () => {
    wrapper.find('.dropzone').first().props().onDragOver();
    expect(wrapper.state('highlight')).toEqual(true);
    wrapper.find('.dropzone').first().props().onDrop();
    expect(wrapper.state('highlight')).toEqual(false);
  });

  it('should not have any state changes when the disabled flag is set', () => {
    wrapper.setProps({ disabled: true });

    wrapper.find('.dropzone').first().props().onDragOver();
    expect(wrapper.state('highlight')).toEqual(false);
  }); 

  it('should call the handleDragOver function upon dragover', () => {
    spy = jest.spyOn(instance, 'handleDragOver');
    instance.forceUpdate();

    wrapper.find('.dropzone').first().props().onDragOver();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('should call the handleDragLeave function upon dragover then leaving the dropzone', () => {
    spy = jest.spyOn(instance, 'handleDragLeave');
    instance.forceUpdate();

    wrapper.find('.dropzone').first().props().onDragLeave();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('should call the handleDrop function upon dropping an item into the dropzone', () => {
    spy = jest.spyOn(instance, 'handleDrop');
    instance.forceUpdate();

    wrapper.find('.dropzone').first().props().onDrop();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('should call the openFileDialog function upon click', () => {
    spy = jest.spyOn(instance, 'openFileDialog');
    instance.forceUpdate();

    wrapper.find('.dropzone').first().props().onClick();
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

describe('<Dropzone /> functions', () => {

  it('should only allow PDF files', () => {
    const fileList = [
      { "name": "sample.pdf", "type": "application/pdf" },
      { "name": "sample.jpg", "type": "image/jpg" },
    ];
    const expResult = [
      { "name": "sample.pdf", "type": "application/pdf" },
    ];

    const result = instance.filterPdfFiles(fileList);
    expect(result).toEqual(expResult);
  });
})
