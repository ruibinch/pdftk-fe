import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Typography } from 'antd';

import './Dropzone.css';

const propTypes = {
  disabled: PropTypes.bool,
  onFilesAdded: PropTypes.func.isRequired,
};

const defaultProps = {
  disabled: false,
};

class Dropzone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      highlight: false,
    };
    this.fileInputRef = React.createRef();
    this.folderInputRef = React.createRef();

    this.handleDragOver =  this.handleDragOver.bind(this);
    this.handleDragLeave = this.handleDragLeave.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
    this.openFileDialog = this.openFileDialog.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
  }

  /***********************************************************************************************/
  /*                                  STATE MANAGEMENT METHODS                                   */
  /***********************************************************************************************/

  handleDragOver(evt) {
    /* Event handler when an item is dragged over the dropzone area. */

    // prevents the default behaviour of the browser
    try { evt.preventDefault(); } catch (e) { }

    if (this.props.disabled) return;
    this.setState({ highlight: true });
  }

  handleDragLeave() {
    /* Event handler when an item is dragged away from the dropzone area. */
    this.setState({ highlight: false });
  }

  handleDrop(evt) {
    /* Event handler when an item is dropped into the dropzone area. */

    try {
      evt.preventDefault();
      if (this.props.disabled) return;

      let files = evt.dataTransfer.files;
      files = this.filterPdfFiles(files);
      this.props.onFilesAdded(files);

      this.resetDropzone(evt);
    } catch (e) { }

    this.setState({ highlight: false });
  }

  /***********************************************************************************************/
  /*                                         HELPER METHODS                                      */
  /***********************************************************************************************/

  filterPdfFiles(files) {
    /* Filters list of uploaded files to only keep files of PDF type. */
    return Array.from(files).filter(file => file.type === 'application/pdf');
  }

  resetDropzone(evt) {
    evt.target.value = null;
  }

  /***********************************************************************************************/
  /*                                   FILE HANDLING METHODS                                     */
  /***********************************************************************************************/

  openFileDialog(evt) {
    /* Opens the file upload dialog for either upload by file or by folder. */

    try {
      if (evt.target.className === 'upload') return; // to prevent default behaviour
      if (this.props.disabled) return;
      
      // Open file upload or folder upload depending if Ctrl key is pressed
      if (evt.ctrlKey) {
        this.folderInputRef.current.click();
      } else {
        this.fileInputRef.current.click();
      }
    } catch (e) { }
  }

  onFilesAdded(evt) {
    /* Triggers a callback to the parent component when new files are added. */
    if (this.props.disabled) return;
    
    let files = evt.target.files;
    files = this.filterPdfFiles(files);
    this.props.onFilesAdded(files);

    this.resetDropzone(evt);
  }

  render() {
    return (
      <>
        <div
          className={`dropzone ${this.state.highlight ? 'highlight' : '' } ${this.props.disabled ? 'disabled' : ''}`}
          onDragOver={this.handleDragOver}
          onDragLeave={this.handleDragLeave}
          onDrop={this.handleDrop}
          onClick={this.openFileDialog}
          style={{ cursor: this.props.disabled ? 'default' : 'pointer' }}
          >
          <Icon style={{ fontSize: '1.5rem' }} type="cloud-upload" />
          <input
            ref={this.folderInputRef}
            className="upload"
            type="file"
            webkitdirectory=""
            directory=""
            multiple
            accept="application/pdf"
            onChange={this.onFilesAdded}
          />
          <input
            ref={this.fileInputRef}
            className="upload"
            type="file"
            multiple
            accept="application/pdf"
            onChange={this.onFilesAdded}
          />
          <span>Upload Files/Folder</span>
        </div>
        <Typography.Text type="secondary">
          Click to upload by file, Ctrl+Click to upload by folder
        </Typography.Text>
      </>
    );
  }
}

Dropzone.propTypes = propTypes;
Dropzone.defaultProps = defaultProps;

export default Dropzone;