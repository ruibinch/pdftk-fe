import React from 'react';
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';

import Sidebar from './../sidebar/Sidebar';
import Header from './../header/Header';
import NotFound from './../notfound/NotFound';
import Home from './../home/Home';
import FSMain from './../factsheet/FSMain';
import TDMain from './../tenderiser/TDMain';
import P2TMain from './../pdf2txt/P2TMain';
import Changelog from './../changelog/Changelog';
import Feedback from './../feedback/Feedback';
import Docs from './../docs/Docs';

import {
  MODULE_HOME,
  URL_HOME,
  MODULE_TD,
  URL_TD,
  MODULE_FS,
  URL_FS,
  MODULE_PDF2TXT,
  URL_PDF2TXT,
  MODULE_CHANGELOG,
  URL_CHANGELOG,
  MODULE_FEEDBACK,
  URL_FEEDBACK,
  URL_DOC_TKFE,
  MODULE_DOC_TKFE,
  URL_DOC_TKBE,
  MODULE_DOC_TKBE,
  URL_DOC_TKDEPLOY,
  MODULE_DOC_TKDEPLOY,
} from './../../constants/Constants';
import 'antd/dist/antd.css';
import './App.css';

// Reference: https://reacttraining.com/react-router/web/example/sidebar
const routes = [
  {
    path: URL_HOME,
    main: () => <Home />,
    sidebar: () => <Sidebar page={MODULE_HOME} />,
  },
  {
    path: URL_TD,
    pageTitle: MODULE_TD,
    main: () => <TDMain />,
    sidebar: () => <Sidebar page={MODULE_TD} />,
    header: () => <Header page={MODULE_TD} />,
  },
  {
    path: URL_FS,
    pageTitle: MODULE_FS,
    main: () => <FSMain />,
    sidebar: () => <Sidebar page={MODULE_FS} />,
    header: () => <Header page={MODULE_FS} />,
  },
  {
    path: URL_PDF2TXT,
    pageTitle: MODULE_PDF2TXT,
    main: () => <P2TMain />,
    sidebar: () => <Sidebar page={MODULE_PDF2TXT} />,
    header: () => <Header page={MODULE_PDF2TXT} />,
  },
  {
    path: URL_CHANGELOG,
    pageTitle: MODULE_CHANGELOG,
    main: () => <Changelog />,
    sidebar: () => <Sidebar page={MODULE_CHANGELOG} />,
    header: () => <Header page={MODULE_CHANGELOG} />,
  },
  {
    path: URL_FEEDBACK,
    pageTitle: MODULE_FEEDBACK,
    main: () => <Feedback />,
    sidebar: () => <Sidebar page={MODULE_FEEDBACK} />,
    header: () => <Header page={MODULE_FEEDBACK} />,
  },
  // Documentation
  {
    path: URL_DOC_TKFE,
    main: () => <Docs page={MODULE_DOC_TKFE} />,
    sidebar: () => <Sidebar page={MODULE_DOC_TKFE} />,
    header: () => <Header page={MODULE_DOC_TKFE} />,
  },
  {
    path: URL_DOC_TKBE,
    main: () => <Docs page={MODULE_DOC_TKBE} />,
    sidebar: () => <Sidebar page={MODULE_DOC_TKBE} />,
    header: () => <Header page={MODULE_DOC_TKBE} />,
  },
  {
    path: URL_DOC_TKDEPLOY,
    main: () => <Docs page={MODULE_DOC_TKDEPLOY} />,
    sidebar: () => <Sidebar page={MODULE_DOC_TKDEPLOY} />,
    header: () => <Header page={MODULE_DOC_TKDEPLOY} />,
  },
];

function App() {
  
  const styleContent = {
    margin: '2rem 1rem 4rem', // top, right left, bottom
  };
    
  return (
    <Router>
      <Route render={({ location }) => (
        <Layout>
          <Switch>
            {routes.map(route => (
              // Sidebar
              <Route
                key={route.path}
                path={route.path}
                component={route.sidebar}
              />
            ))}
            <Route component={Sidebar} />
          </Switch>

          <Layout style={{ background: '#fff' }}>
            <Switch>
              {routes.map(route => (
                // Header
                <Route
                  key={route.path}
                  path={route.path}
                  component={route.header}
                />
              ))}
            </Switch>

            <Layout.Content style={styleContent}>
              <Switch location={location}>
                <Route exact path="/" render={() => (
                  <Redirect to={URL_HOME} />
                )} />
                {routes.map(route => (
                  // Main segment
                  <Route
                    // to force page refresh if sidebar item is clicked when already on that page
                    key={Math.random()}
                    path={route.path}
                    component={route.main}
                  />
                ))}
                <Route component={NotFound} />
              </Switch>
            </Layout.Content>
          </Layout>
        </Layout>
      )} />
    </Router>
  );
}

export default App;