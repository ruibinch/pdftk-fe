import React from 'react';
import { Typography, Row, Col, Card, Divider, Icon, List, Comment, Avatar } from 'antd';
import { Link } from 'react-router-dom';

import {
  URL_FS,
  URL_TD,
  URL_PDF2TXT,
  URL_DOC_TKFE,
  URL_DOC_TKBE,
  URL_DOC_TKDEPLOY,
} from './../../constants/Constants';

const docPages = [
  {
    icon: <Icon type="laptop"/>,
    title: 'Frontend',
    url: URL_DOC_TKFE,
  },
  {
    icon: <Icon type="code"/>,
    title: 'Backend',
    url: URL_DOC_TKBE,
  },
  {
    icon: <Icon type="deployment-unit"/>,
    title: 'Deployment',
    url: URL_DOC_TKDEPLOY,
  },
];

const gridStyle = {
  width: '100%',
  padding: '0 1rem',
};

function Home() {
  return (
    <>
      <div class="mg-b-small"></div>
      <Row type="flex" justify="center">
        <Col span={12} offset={6}>
          <Typography.Title>PDF Toolkit</Typography.Title>
        </Col>
      </Row>
      <div class="mg-b-med"></div>
      <Row>
        <Col span={22} offset={1}>
          <Divider />
        </Col>
      </Row>

      <Row>
        <Col span={20} offset={2}>
          <Typography.Title level={3}>Tools</Typography.Title>
        </Col>
      </Row>
      <div class="mg-b-med"></div>
      <Row>
        <Col span={6} offset={2}>
          <Link to={URL_TD}>
            <Card hoverable>
              <Card.Meta
                avatar={<Icon type="gold"/>}
                title="Tenderiser"
                description="Search for keywords in tender documents"
              />
            </Card>
          </Link>
        </Col>
        <Col span={6} offset={1}>
          <Link to={URL_FS}>
            <Card hoverable>
              <Card.Meta
                avatar={<Icon type="file-search"/>}
                title="Factsheet Searcher"
                description="Find the important information in a RFQ/factsheet document"
              />
            </Card>
          </Link>
        </Col>
        <Col span={6} offset={1}>
          <Link to={URL_PDF2TXT}>
            <Card hoverable>
              <Card.Meta
                avatar={<Icon type="double-right"/>}
                title="PDF-to-text"
                description="Convert .pdf to .txt"
              />
            </Card>
          </Link>
        </Col>
      </Row>
      <div class="mg-b-med"></div>
      <Row>
        <Col span={22} offset={1}>
          <Divider />
        </Col>
      </Row>

      <Row>
        <Col span={20} offset={2}>
          <Typography.Title level={3}>Technical Documentation</Typography.Title>
        </Col>
      </Row>
      <div class="mg-b-med"></div>
      <Row>
        <Col span={20} offset={2}>
          {docPages.map(page => (
            <Card.Grid
              key={page.title}
              style={gridStyle}
            >
              <Link to={page.url}>
                <List.Item>
                  <List.Item.Meta
                    avatar={
                      <Avatar
                        shape="square"
                        style={{ backgroundColor: '#4A4E69' }}
                        icon={page.icon}
                      />
                    }
                    title={page.title}
                  />
                </List.Item>
              </Link>
            </Card.Grid>
          ))}
        </Col>
      </Row>
    </>
  );
}

export default Home;