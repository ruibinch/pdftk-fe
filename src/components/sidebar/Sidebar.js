import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';

import {
  MODULE_HOME,
  URL_HOME,
  MODULE_TD,
  URL_TD,
  MODULE_FS,
  URL_FS,
  MODULE_PDF2TXT,
  URL_PDF2TXT,
  MODULE_CHANGELOG,
  URL_CHANGELOG,
  MODULE_FEEDBACK,
  URL_FEEDBACK,
  MODULE_DOC_TKFE,
  URL_DOC_TKFE,
  MODULE_DOC_TKBE,
  MODULE_DOC_TKDEPLOY,
  URL_DOC_TKDEPLOY,
  URL_DOC_TKBE,
} from './../../constants/Constants';

const propTypes = {
  page: PropTypes.string,
};

const defaultProps = {
  page: '',
};

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  onCollapse = collapsed => this.setState({ collapsed: collapsed });
  
  render() {
    const { page } = this.props;

    const styleSidebar = {
      overflow: 'auto',
      height: '100vh',
      position: 'sticky',
      top: 0,
      left: 0,
    };

    return (
      <Layout.Sider
        style={styleSidebar}
        breakpoint="md"
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div class="logo">Logo</div>
        <Menu theme="dark" mode="inline" selectedKeys={[page]}>
          <Menu.Item key={MODULE_HOME}>
            <Link to={URL_HOME}>
              <Icon type="home"/>
              <span class="nav-text">{MODULE_HOME}</span>
            </Link>
          </Menu.Item>
          <Menu.ItemGroup title="TOOLS">
            <Menu.Item key={MODULE_TD}>
              <Link to={URL_TD}>
                <Icon type="gold"/>
                <span class="nav-text">{MODULE_TD}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={MODULE_FS}>
              <Link to={URL_FS}>
                <Icon type="file-search"/>
                <span class="nav-text">{MODULE_FS}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={MODULE_PDF2TXT}>
              <Link to={URL_PDF2TXT}>
                <Icon type="double-right"/>
                <span class="nav-text">{MODULE_PDF2TXT}</span>
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>

          <Menu.ItemGroup title="MISC">
            <Menu.Item key={MODULE_CHANGELOG}>
              <Link to={URL_CHANGELOG}>
                <Icon type="profile"/>
                <span class="nav-text">{MODULE_CHANGELOG}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={MODULE_FEEDBACK}>
              <Link to={URL_FEEDBACK}>
                <Icon type="team" />
                <span class="nav-text">{MODULE_FEEDBACK}</span>
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>

          <Menu.ItemGroup
            title="DOCS"
            hidden={!page.startsWith('Documentation')}
          >
            <Menu.Item key={MODULE_DOC_TKFE}>
              <Link to={URL_DOC_TKFE}>
                <Icon type="laptop"/>
                <span class="nav-text">{MODULE_DOC_TKFE.split(' ').splice(-1)}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={MODULE_DOC_TKBE}>
              <Link to={URL_DOC_TKBE}>
                <Icon type="code" />
                <span class="nav-text">{MODULE_DOC_TKBE.split(' ').splice(-1)}</span>
              </Link>
            </Menu.Item>
            <Menu.Item key={MODULE_DOC_TKDEPLOY}>
              <Link to={URL_DOC_TKDEPLOY}>
                <Icon type="deployment-unit" />
                <span class="nav-text">{MODULE_DOC_TKDEPLOY.split(' ').splice(-1)}</span>
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>
        </Menu>
      </Layout.Sider>
    );
  }
}

Sidebar.propTypes = propTypes;
Sidebar.defaultProps = defaultProps;

export default Sidebar;