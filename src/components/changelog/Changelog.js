import React from 'react';
import { Row, Col, Timeline, Typography, message } from 'antd';
import { getChangelog } from './../../utils/Ajax';
import { MSG_ERROR_CHANGELOG } from './../../constants/Constants';

const { Paragraph, Text, Title } = Typography;

class Changelog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changelogList: [],
    };

    this.handleUpdateChangelogList = this.handleUpdateChangelogList.bind(this);
  }

  /***********************************************************************************************/
  /*                                    HELPER METHODS                                           */
  /***********************************************************************************************/

  handleUpdateChangelogList(changelogList) {
    this.setState({ changelogList: changelogList });
  }

  sortChangelogList(changelogList) {
    /**
     * Sort changelog list by:
     * 1. ID in descending order (more recent entry first)
     */
    changelogList.sort((a, b) => a.id < b.id ? 1 : -1);
  }

  /***********************************************************************************************/
  /*                                          AJAX CALLS                                         */
  /***********************************************************************************************/

  async populateChangelogList() {
    const promise = getChangelog();

    try {
      let changelogList = await promise;
      this.sortChangelogList(changelogList);
      this.handleUpdateChangelogList(changelogList);
    } catch (e) {
      message.error(MSG_ERROR_CHANGELOG);
    }
  }

  /***********************************************************************************************/
  /*                              COMPONENT LIFECYCLE METHODS                                    */
  /***********************************************************************************************/

  componentDidMount() {
    this.populateChangelogList();
  }

  render() {
    const mappingChangelogType = {
      'major': {
        'color': '#21897E',
        'level': 2,
      },
      'minor': {
        'color': '#0015D5',
        'level': 3,
      },
      'patch': {
        'color': '#7692FF',
        'level': 4,
      },
    };
    
    return (
      <>
        <Row>
          <div class="mg-b-large"></div>
          <Col span={16} offset={4}>
            <Timeline>
              {this.state.changelogList.map(entry => (
                <Timeline.Item
                  key={entry.id}
                  color={mappingChangelogType[entry.type]['color']}
                >
                  <Title level={mappingChangelogType[entry.type]['level']}>
                    {entry.version}
                  </Title>
                  <Text strong>{entry.date}</Text>
                  <div class="mg-b-med"></div>
                  <Paragraph>
                    <ul>
                      {entry.content.map(line => <li key={line}>{line}</li>)}
                    </ul>
                  </Paragraph>
                </Timeline.Item>
              ))}
            </Timeline>
          </Col>
        </Row>
        <div class="mg-b-large"></div>
      </>
    );
  }
}

export default Changelog;