import React from 'react';
import { shallow } from 'enzyme';
import Changelog from './Changelog';

let wrapper, instance, spy;

const changelogList = [
  { 'id': 1, 'date': '111', 'version': 'v1.0.0', 'type': 'major', 'content': ['entry1'] },
  { 'id': 2, 'date': '222', 'version': 'v1.1.0', 'type': 'minor', 'content': ['entry2'] },
  { 'id': 3, 'date': '333', 'version': 'v1.1.1', 'type': 'patch', 'content': ['entry3'] },
];

beforeEach(() => {
  wrapper = shallow(<Changelog />);
  wrapper.setState({ changelogList: changelogList });
  instance = wrapper.instance();
});

describe('<Changelog /> rendering', () => {

  it('should render 1 <Timeline>', () => {
    expect(wrapper.find('Timeline')).toHaveLength(1);
  });
  
  it('should render the same number of  <Timeline.Item> as length of changelogList', () => {
    expect(wrapper.find('TimelineItem')).toHaveLength(changelogList.length);
  });
});

describe('<Changelog /> lifecycle method invocations', () => {

  it('should call populateChangelogList on mount', () => {
    spy = jest.spyOn(Changelog.prototype, 'populateChangelogList');
    wrapper = shallow(<Changelog />);
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

describe('<Changelog /> functions', () => {

  it('should update changelogList state when handleUpdateChangelogList function is called', () => {
    expect(instance.state.changelogList).toEqual(changelogList);
    const changelogListNew = [
      { 'id': 3, 'date': '333', 'version': 'v1.1.1', 'type': 'patch', 'content': ['entry3'] }
    ];
    instance.handleUpdateChangelogList(changelogListNew);
    expect(instance.state.changelogList).toEqual(changelogListNew);
  });

  it('should be sorted by ID in descending order', () => {
      const testList = [{'id': 1, 'entry': 'a'}, {'id': 2, 'entry': 'b'}, {'id': 3, 'entry': 'c'}];
      const sortedTestList = [{'id': 3, 'entry': 'c'}, {'id': 2, 'entry': 'b'}, {'id': 1, 'entry': 'a'}];
      
      instance.sortChangelogList(testList);
      expect(testList).toEqual(sortedTestList);
  });
});
