import React from 'react';
import { shallow } from 'enzyme';
import P2TMain from './P2TMain';

let wrapper, instance, spy;

beforeEach(() => {
  wrapper = shallow(<P2TMain />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
});

describe('<P2TMain /> rendering', () => {

  it('should render 1 <StepsWrapper>', () => {
    expect(wrapper.find('StepsWrapper')).toHaveLength(1);
  });
  
  it('should render 1 <P2TUpload>', () => {
    expect(wrapper.find('P2TUpload')).toHaveLength(1);
  });
  
  it('should render 1 <P2TResults>', () => {
    expect(wrapper.find('P2TResults')).toHaveLength(1);
  });
});

describe('<P2TMain /> functions', () => {

  it('should change stepActive state when handleChangeActiveStep is called', () => {
    wrapper.setState({ stepActive: 2 });

    instance.handleChangeActiveStep(-1);
    expect(instance.state.stepActive).toEqual(1);
    
    instance.handleChangeActiveStep(2);
    expect(instance.state.stepActive).toEqual(3);
  });

  it('should update inProgress state when handleChangeInProgress function is called', () => {
    const testValue = true;
    instance.handleChangeInProgress(testValue);
    expect(instance.state.inProgress).toEqual(testValue);
  });

  it('should update resultsRaw state when handleChangeFilter function is called', () => {
    const testValue = {'result': 'test'};
    instance.handleChangeResultsRaw(testValue);
    expect(instance.state.resultsRaw).toEqual(testValue);
  });
});