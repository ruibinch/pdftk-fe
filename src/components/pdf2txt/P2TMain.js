import React from 'react';
import { Divider } from 'antd';

import { STR_UPLOAD, STR_RESULTS } from './../../constants/Constants';
import StepsWrapper from './../steps/StepsWrapper';
import P2TUpload from './upload/P2TUpload';
import P2TResults from './results/P2TResults';

class P2TMain extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      stepActive: 0,
      steps: [
        { 'icon': 'upload', 'title': STR_UPLOAD },
        { 'icon': 'file-done', 'title': STR_RESULTS },
      ],
      inProgress: false,
      resultsRaw: {},
    };

    this.handleChangeActiveStep = this.handleChangeActiveStep.bind(this);
    this.handleChangeInProgress = this.handleChangeInProgress.bind(this);
    this.handleChangeResultsRaw = this.handleChangeResultsRaw.bind(this);
  }
  
  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeActiveStep(deltaStep) {
    let newStepActive = this.state.stepActive + deltaStep;
    this.setState({ stepActive: newStepActive });
  }

  handleChangeInProgress(inProgress) {
    this.setState({ inProgress: inProgress });
  }

  handleChangeResultsRaw(resultsRaw) {
    this.setState({ resultsRaw: resultsRaw });
  }

  render() {
    return (
      <>
        <StepsWrapper
          steps={this.state.steps}
          stepActive={this.state.stepActive}
          disabled={this.state.inProgress}
          onChangeStep={this.handleChangeActiveStep}
        />
        <Divider />
        <P2TUpload
          hidden={this.state.stepActive !== 0}
          inProgress={this.state.inProgress}
          onChangeActiveStep={this.handleChangeActiveStep}
          onChangeInProgress={this.handleChangeInProgress}
          onChangeResultsRaw={this.handleChangeResultsRaw}
        />
        <P2TResults
          hidden={this.state.stepActive !== 1}
          resultsRaw={this.state.resultsRaw}
          onChangeActiveStep={this.handleChangeActiveStep}
        />
      </>
    );
  }
}

export default P2TMain;