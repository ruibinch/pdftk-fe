import React from 'react';
import { shallow } from 'enzyme';
import P2TResults from './P2TResults';

const mockOnChangeActiveStep = jest.fn();

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    hidden: false,
    resultsRaw: {},
    onChangeActiveStep: mockOnChangeActiveStep,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<P2TResults {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeActiveStep.mockClear();
});

describe('<P2TResults /> rendering', () => {

  it('should render 1 <Collapse />', () => {
    expect(wrapper.find('Collapse')).toHaveLength(1);
  });

  it('should render 2 <Button />', () => {
    expect(wrapper.find('Button')).toHaveLength(2);
  });
});

describe('<P2TResults /> interactions', () => {

  it('should call the downloadAllTexts function when Download All button is clicked', () => {
    spy = jest.spyOn(instance, 'downloadAllTexts');
    instance.forceUpdate();

    wrapper.find('Button').first().props().onClick();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call the onChangeActiveStep props function when Back button is clicked', () => {
    wrapper.find('Button').last().props().onClick();
    expect(mockOnChangeActiveStep).toHaveBeenCalledTimes(1);
    expect(mockOnChangeActiveStep).toHaveBeenCalledWith(-1);
  });
});

describe('<FSResults /> lifecycle method invocations', () => {

  it('should call processResults when resultsRaw props is updated', () => {
    spy = jest.spyOn(instance, 'processResults');
    instance.forceUpdate();

    wrapper.setProps({ resultsRaw: { 'test.pdf': 'test test text text' } });
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

describe('<P2TResults /> functions', () => {

  // TODO: to add
});