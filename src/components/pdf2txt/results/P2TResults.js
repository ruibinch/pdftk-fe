import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, Divider, Collapse, Icon } from 'antd';

const propTypes = {
  hidden: PropTypes.bool.isRequired,
  resultsRaw: PropTypes.object,
  onChangeActiveStep: PropTypes.func.isRequired,
};

const defaultProps = {
  resultsRaw: {},
};

class P2TResults extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
    };
    
    this.handleNavBack = this.handleNavBack.bind(this);
    this.downloadAllTexts = this.downloadAllTexts.bind(this);
  }
  
  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/
  
  handleChangeResults(results) {
    this.setState({ results: results });
  }
  
  handleNavBack() {
    this.props.onChangeActiveStep(-1);
  }

  /***********************************************************************************************/
  /*                                        HELPER METHODS                                       */
  /***********************************************************************************************/

  processText(text) {
    /* Adds HTML <br/> tags to the text for nicer formatting */

    // Remove leading/trailing whitespace
    text = text.trim();
    // Truncate sequences of 3 or more \n chars to just 2 \n chars
    text = text.replace(/(\n){3,}/g, '\n\n');
    // Convert each \n char to a <br/> char
    text = text.replace(/(\n)/g, '<br/>');

    return text;
  }

  processResults() {
    /* Converts the raw results to an array where each element represents 1 PDF file */

    const { resultsRaw } = this.props;

    let results = Object.keys(resultsRaw).map(filename => {
      return {
        filename: filename,
        text: this.processText(resultsRaw[filename]),
      };
    });

    this.handleChangeResults(results);
  }

  /***********************************************************************************************/
  /*                                       DOWNLOAD METHODS                                      */
  /***********************************************************************************************/

  /* Generates a download icon on the right end of the panel */
  renderDownloadIcon = idx => (
    <Icon
      index={idx}
      type="download"
      onClick={event => {
        event.stopPropagation(); // prevents panel from appearing/hiding

        const selIndex = event.currentTarget.getAttribute('index');
        this.downloadText(this.state.results[selIndex]);
      }}
    />
  );

  downloadAllTexts() {
    /* Triggers a download of all texts */

    this.state.results.forEach(result => {
      this.downloadText(result);
    });
  }

  downloadText(result) {
    /* Triggers a download of the text belonging to the selected result */

    const filename = result['filename'];
    const text = result['text'].replace(/(<br\/>)/g, '\n')

    // Create an <a> element to take advantage of the download attribute
    const element = document.createElement('a');
    element.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(text)}`);
    element.setAttribute('download', `${filename.split('.')[0]}.txt`);
    element.style.display = 'none';

    // Add the <a> element to the document and simulate a click event
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  /***********************************************************************************************/
  /*                                 COMPONENT LIFECCLE METHODS                                  */
  /***********************************************************************************************/
  
  componentDidUpdate(prevProps) {
    if (prevProps.resultsRaw !== this.props.resultsRaw) {
      this.processResults();
    }
  };


  render() {
    return (
      <div hidden={this.props.hidden}>
        <Row>
          <Col span={22} offset={1}>
            <Button
              type="primary"
              icon="download"
              hidden={this.state.results.length === 0}
              onClick={this.downloadAllTexts}
            >
              Download All
            </Button>
            <div class="pad-b-med"></div>
            <Collapse accordion>
              {this.state.results.map((result, idx) => (
                <Collapse.Panel
                  header={result.filename}
                  key={result.filename}
                  forceRender={true}  
                  extra={this.renderDownloadIcon(idx)}
                >
                  <p dangerouslySetInnerHTML={{__html: result.text}}></p>
                </Collapse.Panel>
              ))}
            </Collapse>
          </Col>
        </Row>     

        <Divider />
        <Row>
          <Col span={3} push={3}>
            <Button onClick={this.handleNavBack}>Back</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

P2TResults.propTypes = propTypes;
P2TResults.defaultProps = defaultProps;

export default P2TResults;