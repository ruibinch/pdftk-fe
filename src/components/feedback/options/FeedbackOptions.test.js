import React from 'react';
import { shallow } from 'enzyme';
import FeedbackOptions from './FeedbackOptions';
import { STR_ALL, STR_POPULARITY } from '../../../constants/Constants';

const mockOnChangeFilterType = jest.fn();
const mockOnChangeSortBy = jest.fn();

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    filterType: STR_ALL,
    sortBy: STR_POPULARITY,
    onChangeFilterType: mockOnChangeFilterType,
    onChangeSortBy: mockOnChangeSortBy,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FeedbackOptions {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnChangeFilterType.mockClear();
  mockOnChangeSortBy.mockClear();
});


describe('<FeedbackOptions /> rendering', () => {

  it('should render 2 <Dropdown>', () => {
      expect(wrapper.find('Dropdown')).toHaveLength(2);
  });
});