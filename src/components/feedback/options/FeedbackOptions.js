import React from 'react';
import PropTypes from 'prop-types';
import { STR_DATE, STR_POPULARITY, STR_ALL, STR_BUG_REPORT, STR_SUGGESTION } from './../../../constants/Constants';
import { Menu, Dropdown, Icon } from 'antd';

const propTypes = {
  filterType: PropTypes.oneOf([STR_ALL, STR_BUG_REPORT, STR_SUGGESTION]).isRequired,
  sortBy: PropTypes.oneOf([STR_DATE, STR_POPULARITY]).isRequired,
  onChangeFilterType: PropTypes.func.isRequired,
  onChangeSortBy: PropTypes.func.isRequired,
};

function FeedbackOptions({ filterType, sortBy, onChangeFilterType, onChangeSortBy }) {

  const handleChangeSortMenu = e => onChangeSortBy(e.key);

  const handleChangeFilterMenu = e => onChangeFilterType(e.key);

  const sortMenu = (
    <Menu
      selectedKeys={[sortBy]}
      onClick={handleChangeSortMenu}
    >
      <Menu.Item key={STR_DATE}>
        By Date
      </Menu.Item>
      <Menu.Item key={STR_POPULARITY}>
        By Popularity
      </Menu.Item>
    </Menu>
  );

  const filterMenu = (
    <Menu
      selectedKeys={[filterType]}
      onClick={handleChangeFilterMenu}
    >
      <Menu.Item key={STR_ALL}>
        All
      </Menu.Item>
      <Menu.Item key={STR_BUG_REPORT}>
        Bug Report
      </Menu.Item>
      <Menu.Item key={STR_SUGGESTION}>
        Suggestion
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Dropdown overlay={sortMenu}>
        <a className="ant-dropdown-link float-right" href="#">
          Sort <Icon type="down" />
        </a>
      </Dropdown>
      <Dropdown overlay={filterMenu}>
        <a className="ant-dropdown-link float-right" href="#" style={{ marginRight: 16 }}>
          Filter <Icon type="down" />
        </a>
      </Dropdown>
    </>
  );
}

FeedbackOptions.propTypes = propTypes;

export default FeedbackOptions;