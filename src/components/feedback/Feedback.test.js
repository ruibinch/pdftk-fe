import React from 'react';
import { shallow } from 'enzyme';
import Feedback from './Feedback';
import { STR_DATE, STR_POPULARITY, STR_BUG_REPORT } from '../../constants/Constants';

let wrapper, instance, spy;

beforeEach(() => {
  wrapper = shallow(<Feedback />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
});

describe('<Feedback /> rendering', () => {

  it('should render 1 <FeedbackForm>', () => {
    expect(wrapper.find('FeedbackForm')).toHaveLength(1);
  });
  
  it('should render 1 <FeedbackOptions>', () => {
    expect(wrapper.find('FeedbackOptions')).toHaveLength(1);
  });
  
  it('should render 1 <FeedbackEntries>', () => {
    expect(wrapper.find('FeedbackEntries')).toHaveLength(1);
  });
});

describe('<Feedback /> lifecycle method invocations', () => {

  it('should call populateFeedbackList on component mounting', () => {
    spy = jest.spyOn(Feedback.prototype, 'populateFeedbackList');
    wrapper = shallow(<Feedback />);
    expect(spy).toHaveBeenCalledTimes(1);
  });
    
  it('should call updateFeedbackListDisplay when there is a change in the "filterType" state', () => {
    spy = jest.spyOn(instance, 'updateFeedbackListDisplay');
    instance.forceUpdate();
    
    wrapper.setState({ filterType: STR_BUG_REPORT });
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('should call updateFeedbackListDisplay when there is a change in the "sortBy" state', () => {
    spy = jest.spyOn(instance, 'updateFeedbackListDisplay');
    instance.forceUpdate();
    
    wrapper.setState({ sortBy: STR_DATE });
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call updateFeedbackListDisplay there is a change in the "feedbackList" state', () => {
    spy = jest.spyOn(instance, 'updateFeedbackListDisplay');
    instance.forceUpdate();
    
    const newFeedbackList = [{'content': 'aaa'}, {'content': 'bbb'}]
    wrapper.setState({ feedbackList: newFeedbackList });
    expect(spy).toHaveBeenCalledTimes(1);
  });
});

describe('<Feedback /> functions', () => {
 
  it('should update connError state when handleChangeConnError function is called', () => {
    instance.handleChangeConnError(true);
    expect(instance.state.connError).toEqual(true);
  });

  it('should update filterType state when handleChangeFilter function is called', () => {
    instance.handleChangeFilterType(STR_BUG_REPORT);
    expect(instance.state.filterType).toEqual(STR_BUG_REPORT);
  });

  it('should update sortBy state when handleChangeSortBy function is called', () => {
    instance.handleChangeSortBy(STR_POPULARITY);
    expect(instance.state.sortBy).toEqual(STR_POPULARITY);
  });

  // TODO: test case for sortFeedbackList
});