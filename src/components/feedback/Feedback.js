import React from 'react';
import { Row, Col, message } from 'antd';
import FeedbackForm from './form/FeedbackForm';
import FeedbackEntries from './entries/FeedbackEntries';
import { getFeedback, putFeedback } from './../../utils/Ajax';
import { STR_DATE, STR_POPULARITY, MSG_ERROR_FEEDBACK, MSG_FEEDBACK_UPDATE_ERROR, STR_ALL } from './../../constants/Constants';
import FeedbackOptions from './options/FeedbackOptions';

class Feedback extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      connError: null,
      feedbackList: [],
      feedbackListDisplay: [],
      filterType: STR_ALL,
      sortBy: STR_POPULARITY,
    };

    this.handleChangeConnError = this.handleChangeConnError.bind(this);
    this.handleChangeFeedbackList = this.handleChangeFeedbackList.bind(this);
    this.handleChangeFeedbackListDisplay = this.handleChangeFeedbackListDisplay.bind(this);
    this.handleChangeFilterType = this.handleChangeFilterType.bind(this);
    this.handleChangeSortBy = this.handleChangeSortBy.bind(this);
    this.updateFeedbackListDisplay = this.updateFeedbackListDisplay.bind(this);
    this.handleVote = this.handleVote.bind(this);
    this.populateFeedbackList = this.populateFeedbackList.bind(this);
  }

  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  handleChangeConnError(connError) {
    this.setState({ connError: connError });
  }

  handleChangeFeedbackList(feedbackList) {
    this.setState({ feedbackList: feedbackList });
  }
  
  handleChangeFeedbackListDisplay(feedbackListDisplay) {
    this.setState({ feedbackListDisplay: feedbackListDisplay });
  }

  handleChangeFilterType(filterType) {
    this.setState({ filterType: filterType }); 
  }

  handleChangeSortBy(sortBy) {
    this.setState({ sortBy: sortBy });
  }
  
  /***********************************************************************************************/
  /*                                    HELPER METHODS                                           */
  /***********************************************************************************************/

  sortFeedbackList(feedbackList) {
    /**
     * 2 possible values for `sortBy`:
     * 1. 'Popularity': sort by score in descending order (default)
     *  - Score is counted by (likes - dislikes)
     *  - If the score is the same, then sort by likes in descending order
     * 2. 'Date': sort by date in descending order
     * 
     * If all are equal, then ID is used as the final level of sorting (more recent entry first).
     */

    if (feedbackList === undefined) return;
    const { sortBy } = this.state;
    
    feedbackList.sort((a, b) => {
      const firstLayerParam1 = sortBy === STR_DATE ? 
        this.strToDate(a.date) : 
        this.getScore(a.likes, a.dislikes);
      const firstLayerParam2 = sortBy === STR_DATE ? 
        this.strToDate(b.date) : 
        this.getScore(b.likes, b.dislikes);

      if (firstLayerParam1 !== firstLayerParam2) {
        return firstLayerParam1 < firstLayerParam2 ? 1 : -1;
      } else {
        if (sortBy === STR_POPULARITY) {
          if (a.likes !== b.likes) {
            return a.likes < b.likes ? 1 : -1;
          } else {
            return a.id < b.id ? 1 : -1;
          }
        }
        return a.id < b.id ? 1 : -1;
      }
    });
  }

  strToDate(date) {
    /* Reads a date in YYYY/MM/DD format and returns a Date object in Unix-time. */
    let dateSplit = date.split('/');
    let dateObj = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]);
    return dateObj.getTime();
  }

  getScore(likes, dislikes) {
    return likes - dislikes;
  }

  handleVote(id, delta) {
    const feedbackList = [...this.state.feedbackList];

    const foundIndex = feedbackList.findIndex(x => x.id === id && x.delta === 0);
    if (foundIndex !== -1) {
      const entryToUpdate = feedbackList[foundIndex];
      const deltaAttribute = delta > 0 ? 'likes' : 'dislikes';
      entryToUpdate['delta'] = delta;
      entryToUpdate[deltaAttribute] += 1;

      const feedbackListUpdated = feedbackList.map(entry => {
        if (entry['id'] === entryToUpdate['id']) {
          return entryToUpdate;
        } else {
          return entry;
        }
      });
      
      this.editFeedbackList(entryToUpdate);
      this.handleChangeFeedbackList(feedbackListUpdated);
    }
  }

  updateFeedbackListDisplay() {
    const { feedbackList, filterType } = this.state;
    const feedbackListDisplay = feedbackList.filter(e => {
      if (filterType === STR_ALL) return true;
      return e['feedbackType'] === filterType;
    });

    this.sortFeedbackList(feedbackListDisplay);
    this.handleChangeFeedbackListDisplay(feedbackListDisplay);
  }

  /***********************************************************************************************/
  /*                                          AJAX CALLS                                         */
  /***********************************************************************************************/

  async populateFeedbackList() {
    /* Pulls the list of existing feedback entries. */

    const promise = getFeedback();

    try {
      let feedbackList = await promise;
      // Sorts the feedback list
      this.sortFeedbackList(feedbackList);
      // Adds a `delta` key with value "0" to all the objects in the array 
      feedbackList.map(e => e.delta = 0);
      
      this.handleChangeConnError(false);
      this.handleChangeFeedbackList(feedbackList);
    } catch (e) {
      this.handleChangeConnError(true);
      message.error(MSG_ERROR_FEEDBACK);
    }
  }

  async editFeedbackList(feedback) {
    /* Edits the feedback entry in the feedback list. */
    if (this.props.disabled) return;

    const promise = putFeedback(feedback);
    try {
      await promise;
    } catch (e) {
      message.error(MSG_FEEDBACK_UPDATE_ERROR);
    }
  }

  /***********************************************************************************************/
  /*                              COMPONENT LIFECYCLE METHODS                                    */
  /***********************************************************************************************/

  componentDidMount() {
    this.populateFeedbackList();
  }
  
  componentDidUpdate(prevProps, prevState) {
    if ((prevState.filterType !== this.state.filterType) || 
        (prevState.sortBy !== this.state.sortBy)) {
      this.updateFeedbackListDisplay();
    }

    if (prevState.feedbackList !== this.state.feedbackList) {
      this.updateFeedbackListDisplay();
    }
  }

  render() {
    return (
      <>
        <Row>
          <Col span={18} offset={3}>
            <FeedbackForm
              disabled={this.state.connError !== false}
              onRefreshFeedbackList={this.populateFeedbackList}
            />
          </Col>
        </Row>
        <div class="mg-b-large"></div>
        <Row type="flex" justify="end">
          <Col span={18} pull={3}  type="flex" justify="end">
            <FeedbackOptions
              filterType={this.state.filterType}
              sortBy={this.state.sortBy}
              onChangeFilterType={this.handleChangeFilterType}
              onChangeSortBy={this.handleChangeSortBy}
            />
          </Col>
        </Row>
        <div class="mg-b-med"></div>
        <Row>
          <Col span={18} offset={3}>
            <FeedbackEntries
              feedbackList={this.state.feedbackListDisplay}
              onVote={this.handleVote}
            />
          </Col>
        </Row>
      </>
    );
  }
}

export default Feedback;