import React from 'react';
import PropTypes from 'prop-types';
import { Comment, Tooltip, Icon, Card, Tag } from 'antd';
import { STR_LIKE, STR_DISLIKE } from './../../../constants/Constants';

const propTypes = {
  feedbackList: PropTypes.array.isRequired,
  onVote: PropTypes.func.isRequired,
};

function FeedbackEntries({ feedbackList, onVote }) {

  const handleVote = event => {
    const id = parseInt(event.currentTarget.id);
    const iconType = event.currentTarget.getAttribute('aria-label').split(':')[1].trim();
    const delta = iconType === STR_LIKE ? 1 : -1;

    onVote(id, delta);
  }

  const gridStyle = {
    width: '100%',
    padding: '0 1rem',
  };

  const feedbackTypeToTagColor = {
    'Bug Report': '#C70039',
    'Suggestion': '#003F93',
  };

  return (
    <>
      {feedbackList.map(entry => (
        <Card.Grid
          key={entry.id}
          style={gridStyle}
        >
          <Comment
            actions={[
              <span key="comment-like">
                <Tooltip title="Like">
                  <Icon
                    type={STR_LIKE}
                    id={entry.id}
                    theme={entry.delta === 1 ? 'filled' : 'outlined'}
                    style={entry.delta === 1 ? { color: 'green' } : { color: '' }}
                    onClick={handleVote}
                  />
                </Tooltip>
                <span style={{ paddingLeft: 8, cursor: 'auto' }}>{entry.likes}</span>
              </span>,
              <span key="comment-dislike">
                <Tooltip title="Dislike">
                  <Icon
                    type={STR_DISLIKE}
                    id={entry.id}
                    theme={entry.delta === -1 ? 'filled' : 'outlined'}
                    style={entry.delta === -1 ? { color: 'red' } : { color: '' }}
                    onClick={handleVote}
                  />
                </Tooltip>
                <span style={{ paddingLeft: 8, cursor: 'auto' }}>{entry.dislikes}</span>
              </span>,
              <span key="comment-tags" style={{ paddingLeft: 16 }}>
                <Tag color={feedbackTypeToTagColor[entry.feedbackType]}>
                  {entry.feedbackType}
                </Tag>
              </span>
            ]}
            author={
              <strong style={{color: 'rgba(0, 0, 0, 0.75)'}}>
                {entry.submitterName}
              </strong>
            }
            content={entry.content}
            datetime={entry.date}
          />
        </Card.Grid>
      ))}
    </>
  );
}

FeedbackEntries.propTypes = propTypes;

export default FeedbackEntries;  