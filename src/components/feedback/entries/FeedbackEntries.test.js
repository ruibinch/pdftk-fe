import React from 'react';
import { shallow } from 'enzyme';
import FeedbackEntries from './FeedbackEntries';

const mockOnVote = jest.fn();

let props, wrapper;

function createTestProps() {
  return {
    feedbackList: [
      {
        "id": 1,
        "likes": 5,
        "dislikes": 0,
        "feedbackType": "Suggestion",
        "content": "Make the font larger",
        "submitterName": "system",
        "date": "05/07/2019",
      },
    ],
    onVote: mockOnVote,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FeedbackEntries {...props} />);
});

afterEach(() => {
  mockOnVote.mockClear();
});

describe('<FeedbackEntries /> rendering', () => {

  it('should render 1 <Grid> ', () => {
    expect(wrapper.find('Grid')).toHaveLength(1);
  });
});

// describe('<FeedbackEntries /> interactions', () => {

//   it('should call the onVote callback when either the Like/Dislike icon is clicked', () => {
//     const event = {
//       'currentTarget': {
//         'id': 1000,
//         'aria-label': 'label: like',
//       },
//     };
//     wrapper.find('Icon').first().props().onClick(event);
//     expect(mockOnVote).toHaveBeenCalledWith(1000, 1);

//     // Change the 'title' attribute
//     event['currentTarget']['aria-label'] = 'label: dislike';
//     wrapper.find('Icon').last().props().onClick(event);
//     expect(mockOnUpdateScore).toHaveBeenCalledWith(1000, -1);
//   });
// });