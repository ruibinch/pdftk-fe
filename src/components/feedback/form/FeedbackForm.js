import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Radio, message, Button } from 'antd';
import { putFeedback } from './../../../utils/Ajax';
import { STR_BUG_REPORT, STR_SUGGESTION, MSG_FIELDS_REQ, MSG_FEEDBACK_SUBMIT_SUCCESS, MSG_FEEDBACK_SUBMIT_ERROR } from './../../../constants/Constants';

const propTypes = {
  disabled: PropTypes.bool.isRequired,
  onRefreshFeedbackList: PropTypes.func.isRequired,
};

class FeedbackForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formVisible: false,
      confirmLoading: false,
      submitterName: '',
      feedbackType: '',
      content: '',
      // isSubmitterNameEmpty: null,
      // isFeedbackTypeEmpty: null,
      // isContentEmpty: null,
    };

    this.showFeedbackForm = this.showFeedbackForm.bind(this);
    this.hideFeedbackForm = this.hideFeedbackForm.bind(this);
    this.handleChangeConfirmLoading = this.handleChangeConfirmLoading.bind(this);
    this.handleChangeSubmitterName = this.handleChangeSubmitterName.bind(this);
    this.handleChangeFeedbackType = this.handleChangeFeedbackType.bind(this);
    this.handleChangeContent = this.handleChangeContent.bind(this);
    this.onClickOk = this.onClickOk.bind(this);
  }
  
  /***********************************************************************************************/
  /*                                 STATE MANAGEMENT METHODS                                    */
  /***********************************************************************************************/

  showFeedbackForm() {
    this.setState({ formVisible: true });
  }
  
  hideFeedbackForm(isClearForm) {
    if (isClearForm === true) {
      this.setState({
        submitterName: '',
        feedbackType: '',
        content: '',
      });
    }
    this.setState({ formVisible: false });
  }
  
  handleChangeConfirmLoading(confirmLoading) {
    this.setState({ confirmLoading: confirmLoading });
  }

  handleChangeSubmitterName(event) {
    if (this.props.disabled) return;
    this.setState({ submitterName: event.target.value });
  }

  handleChangeFeedbackType(event) {
    if (this.props.disabled) return;
    this.setState({ feedbackType: event.target.value });
  }

  handleChangeContent(event) {
    if (this.props.disabled) return;
    this.setState({ content: event.target.value });
  }

  /***********************************************************************************************/
  /*                                        HELPER METHODS                                       */
  /***********************************************************************************************/

  onClickOk() {
    if (this.props.disabled) return;
    
    const feedback = {
      submitterName: this.state.submitterName,
      feedbackType: this.state.feedbackType,
      content: this.state.content,
      likes: 0,
      dislikes: 0,
    };

    this.addToFeedbackList(feedback);
  }
    
  validateFeedbackEntryDetails(feedback) {
    /* Validates the input details of the feedback entry. */

    const isSubmitterNameEmpty = feedback['submitterName'] === '';
    const isFeedbackTypeEmpty = feedback['feedbackType'] === '';
    const isContentEmpty = feedback['content'] === '';

    if (isSubmitterNameEmpty || isFeedbackTypeEmpty || isContentEmpty) {
      message.error(MSG_FIELDS_REQ);
      return false;
    }
    return true;
  }

  /***********************************************************************************************/
  /*                                          AJAX CALLS                                         */
  /***********************************************************************************************/
  
  async addToFeedbackList(feedback) {
    /* Adds the new feedback entry to the feedback list. */
    if (!this.validateFeedbackEntryDetails(feedback)) return;

    const promise = putFeedback(feedback);
    this.handleChangeConfirmLoading(true);

    try {
      await promise;
      this.handleChangeConfirmLoading(false);

      // on success
      this.hideFeedbackForm(true);
      this.props.onRefreshFeedbackList();
      message.success(MSG_FEEDBACK_SUBMIT_SUCCESS);
    } catch (e) {
      message.error(MSG_FEEDBACK_SUBMIT_ERROR);
    }

    this.handleChangeConfirmLoading(false);
  }

  render() {
    return (
      <>
        <Button type="primary" onClick={this.showFeedbackForm}>
          Submit Feedback
        </Button>
        <Modal
          visible={this.state.formVisible}
          title="Submit Feedback"
          okText="Submit"
          onOk={this.onClickOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.hideFeedbackForm}
        >
          <Form layout="vertical">
            <Form.Item
              label="Your Name"
              required
            >
              <Input
                disabled={this.props.disabled}
                value={this.state.submitterName}
                onChange={this.handleChangeSubmitterName}
              />
            </Form.Item>
            <Form.Item
              label="Feedback Type"
              required
            >
              <Radio.Group
                disabled={this.props.disabled}
                value={this.state.feedbackType}
                onChange={this.handleChangeFeedbackType}
              >
                <Radio value={STR_BUG_REPORT}>{STR_BUG_REPORT}</Radio>
                <Radio value={STR_SUGGESTION}>{STR_SUGGESTION}</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label="Content"
              required
            >
              <Input.TextArea
                rows={5}
                disabled={this.props.disabled}
                value={this.state.content}
                onChange={this.handleChangeContent}
              />
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }
}

FeedbackForm.propTypes = propTypes;

export default FeedbackForm;