import React from 'react';
import { shallow } from 'enzyme';
import FeedbackForm from './FeedbackForm';

const mockOnRefreshFeedbackList = jest.fn();

let props, wrapper, instance, spy;

function createTestProps() {
  return {
    disabled: false,
    onRefreshFeedbackList: mockOnRefreshFeedbackList,
  };
}

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<FeedbackForm {...props} />);
  instance = wrapper.instance();
});

afterEach(() => {
  if (spy !== undefined) spy.mockClear();
  mockOnRefreshFeedbackList.mockClear();
});

describe('<FeedbackForm /> rendering', () => {

  it('should render 1 <Button>', () => {
      expect(wrapper.find('Button')).toHaveLength(1);
  });
  
  it('should render 1 <Modal>', () => {
      expect(wrapper.find('Modal')).toHaveLength(1);
  });
  
  it('should render 1 <FormItem>', () => {
      expect(wrapper.find('FormItem')).toHaveLength(3);
  });
});

describe('<FeedbackForm /> interactions', () => {

  it('clicking the "Submit Feedback" button should display the modal', () => {
    wrapper.find('Button').first().props().onClick();
    expect(instance.state.formVisible).toEqual(true);
  });
  
  it('closing the form should hide the modal', () => {
    wrapper.find('Modal').first().props().onCancel();
    expect(instance.state.formVisible).toEqual(false);
  });
  
  it('should update submitterName state when there is a change in the "Your Name" input field', () => {
    const testValue = 'sample_name';
    const event = { 'target': { 'value': testValue } };

    wrapper.find('Input').first().props().onChange(event);
    expect(instance.state.submitterName).toEqual(testValue);
  });

  it('should update feedbackType state when there is a change in the "Feedback Type" input field', () => {
    const testValue = 'sample_feedback_type';
    const event = { 'target': { 'value': testValue } };

    wrapper.find('RadioGroup').first().props().onChange(event);
    expect(instance.state.feedbackType).toEqual(testValue);
  });

  it('should update content state when there is a change in the "Content" input field', () => {
    const testValue = 'sample_content';
    const event = { 'target': { 'value': testValue } };

    wrapper.find('TextArea').props().onChange(event);
    expect(instance.state.content).toEqual(testValue);
  });
  
  it('if disabled in props is true, the form input elements should be unresponsive', () => {
    wrapper.setProps({ disabled: true });
    
    // Changing the "Your Name" field
    const event_name = { 'target': { 'value': 'sample_name' } };
    wrapper.find('Input').first().props().onChange(event_name);
    expect(instance.state.submitterName).toEqual('');

    // Changing the "Feedback Type" field
    const event_feedback_type = { 'target': { 'value': 'sample_feedback_type' } };
    wrapper.find('RadioGroup').first().props().onChange(event_feedback_type);
    expect(instance.state.feedbackType).toEqual('');

    // Changing the "Content" field
    const event_content = { 'target': { 'value': 'sample_content' } };
    wrapper.find('TextArea').props().onChange(event_content);
    expect(instance.state.content).toEqual('');

    // Clicking the OK button should not trigger a function call to addToFeedbackList
    wrapper.find('Modal').first().props().onOk();
    expect(mockOnRefreshFeedbackList).toHaveBeenCalledTimes(0);
  });
  
  it('clicking the "Submit" button should call the addToFeedbackList function', () => {
    spy = jest.spyOn(instance, 'addToFeedbackList');
    instance.forceUpdate();
    
    wrapper.find('Modal').first().props().onOk();
    expect(spy).toHaveBeenCalledTimes(1);
  });
  
  it('clicking the "Submit" button should not call the addToFeedbackList function, if disabled is true', () => {
    wrapper.setProps({ disabled: true });
    spy = jest.spyOn(instance, 'addToFeedbackList');
    instance.forceUpdate();
    
    wrapper.find('Modal').first().props().onOk();
    expect(spy).toHaveBeenCalledTimes(0);
  });
});

describe('<FeedbackForm /> functions', () => {

  it('validateFeedbackEntryDetails should return true when all required fields are filled', () => {
    const feedback = {
      submitterName: 'test',
      feedbackType: 'test',
      content: 'test',
    };

    const returnValue = instance.validateFeedbackEntryDetails(feedback);
    expect(returnValue).toEqual(true);
  });
  
  it('validateFeedbackEntryDetails should return false when submitterName field is empty', () => {
    const feedback = {
      submitterName: '',
      feedbackType: 'test',
      content: 'test',
    };

    const returnValue = instance.validateFeedbackEntryDetails(feedback);
    expect(returnValue).toEqual(false);
  });
  
  it('validateFeedbackEntryDetails should return false when feedbackType field is empty', () => {
    const feedback = {
      submitterName: 'test',
      feedbackType: '',
      content: 'test',
    };

    const returnValue = instance.validateFeedbackEntryDetails(feedback);
    expect(returnValue).toEqual(false);
  });
  
  it('validateFeedbackEntryDetails should return false when content field is empty', () => {
    const feedback = {
      submitterName: 'test',
      feedbackType: 'test',
      content: '',
    };

    const returnValue = instance.validateFeedbackEntryDetails(feedback);
    expect(returnValue).toEqual(false);
  });
});