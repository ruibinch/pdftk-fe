import React from 'react';
import PropTypes from 'prop-types';
import { Steps, Icon, Row, Col } from 'antd';

const propTypes = {
  steps: PropTypes.array.isRequired,
  stepActive: PropTypes.number.isRequired,
  disabled: PropTypes.bool.isRequired,
  onChangeStep: PropTypes.func.isRequired,
};

function StepsWrapper({ steps, stepActive, disabled, onChangeStep }) {
  // The span and offset settings for Col should vary depending on the number of steps
  let colSpan = steps.length >= 4 ? 18 : steps.length === 3 ? 16 : 14;
  let colOffset = steps.length >= 4 ? 3 : steps.length === 3 ? 4 : 5;

  const handleChangeStep = (newStep) => {
    let deltaStep = newStep - stepActive;
    onChangeStep(deltaStep);
  };

  return (
    <>
      <Row>
        <Col span={colSpan} offset={colOffset}>
          <Steps
            size="small"
            current={stepActive}
            onChange={handleChangeStep}
          >
            {steps.map(item => (
              <Steps.Step
                key={item.title}
                icon={<Icon type={item.icon} />}
                title={item.title}
                disabled={disabled}
              />
            ))}
          </Steps>
        </Col>
      </Row>
    </>
  );
}

StepsWrapper.propTypes = propTypes;

export default StepsWrapper;